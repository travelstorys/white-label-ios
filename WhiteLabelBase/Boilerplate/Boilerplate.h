//
//  Boilerplate.h
//  WhiteLabelBase
//
//  Created by David Worth on 3/19/19.
//  Copyright © 2019 TravelStorysGPS, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

#import <TravelStorysExtSDK/TSGUIColor+Hex.h>
#import <TravelStorysExtSDK/TSGBoilerViewSupport.h>

NS_ASSUME_NONNULL_BEGIN

@interface Boilerplate : NSObject

+ (void)setConfig:(NSDictionary*)config;
+ (id)getObjectValue:(TSGBoilerplateValue)option;
+ (int)getIntegerValue:(TSGBoilerplateValue)option;

@end

NS_ASSUME_NONNULL_END
