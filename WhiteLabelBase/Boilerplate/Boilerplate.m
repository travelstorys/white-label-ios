//
//  Boilerplate.m
//  WhiteLabelBase
//
//  Created by David Worth on 3/19/19.
//  Copyright © 2019 TravelStorysGPS, LLC. All rights reserved.
//

#import "Boilerplate.h"

@implementation Boilerplate

static NSDictionary* plate;

+ (void)setConfig:(NSDictionary *)config
{
    plate = config;
}

+ (id)getObjectValue:(TSGBoilerplateValue)option
{
    if(option == BoilerLogo)
        return plate[@"logo"];
    else if(option == BoilerLogoTrans)
        return plate[@"logo_trans"];
    else if(option == BoilerLaunchLogo)
        return plate[@"launch"][@"logo"];
    else if(option == BoilerLaunchTitle)
        return plate[@"launch"][@"title"];
    else if(option == BoilerLaunchSubtitle)
        return plate[@"launch"][@"subtitle"];
    
    else if(option == BoilerColorPrimary)
        return [UIColor colorWithHex:plate[@"colors"][@"primary"]];
    else if(option == BoilerColorSecondary)
        return [UIColor colorWithHex:plate[@"colors"][@"secondary"]];
    else if(option == BoilerColorTertiary)
        return [UIColor colorWithHex:plate[@"colors"][@"tertiary"]];
    else if(option == BoilerColorNeutral)
        return [UIColor colorWithHex:plate[@"colors"][@"neutral"]];
    else if(option == BoilerColorNeutral2)
        return [UIColor colorWithHex:plate[@"colors"][@"neutral2"]];
    
    else if(option == BoilerPermissions)
        return plate[@"permissions"][@"color order"];
    else if(option == BoilerPermissionsWelcome)
        return plate[@"permissions"][@"welcome"];
    
    else if(option == BoilerDiscoveryBackground)
        return plate[@"discovery"][@"background"];
    
    else if(option == BoilerOptionsBackground)
        return plate[@"options"][@"background"];
    else if(option == BoilerOptionsContactURL)
        return plate[@"options"][@"contactURL"];
    else if(option == BoilerOptionsPrivacyURL)
        return plate[@"options"][@"privacyURL"];
    else if(option == BoilerOptionsTermsURL)
        return plate[@"options"][@"termsURL"];
    
    else if(option == BoilerAssetDownloadSmall)
        return plate[@"replacedAssets"][@"downloadSmall"];
    else if(option == BoilerAssetDownloadMedium)
        return plate[@"replacedAssets"][@"downloadMedium"];
    else if(option == BoilerAssetPlaySmall)
        return plate[@"replacedAssets"][@"playSmall"];
    else if(option == BoilerAssetPlayMedium)
        return plate[@"replacedAssets"][@"playMedium"];
    
    else if(option == BoilerTSGDeepLinkScheme)
        return plate[@"TSGDeepLinkScheme"];
    
    //Option unsupported
    NSLog(@"Boilerplate option unsupported for id type");
    return nil;
}

+ (int)getIntegerValue:(TSGBoilerplateValue)option
{
    if(option == BoilerLaunchDarkMode)
    {
        return [plate[@"launch"][@"darkMode"] intValue];
    }
    
    NSLog(@"Boilerplate option unsupported for integer type");
    return -1;
}

@end
