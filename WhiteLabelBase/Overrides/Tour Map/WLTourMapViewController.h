//
//  WLTourMapViewController.h
//  WhiteLabelBase
//
//  Created by David Worth on 4/29/19.
//  Copyright © 2019 TravelStorysGPS, LLC. All rights reserved.
//

#import <TravelStorysExtSDK/TravelStorysExtSDK.h>

#import "WLTourGeotagViewController.h"
#import "WLTourAboutViewController.h"
#import "WLTourConnectViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface WLTourMapViewController : TSGTourMapViewController

- (void)annotationClicked:(TSGGeotagAnnotation *)annotation;

@end

NS_ASSUME_NONNULL_END
