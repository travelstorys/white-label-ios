//
//  WLTourMapViewController.m
//  WhiteLabelBase
//
//  Created by David Worth on 4/29/19.
//  Copyright © 2019 TravelStorysGPS, LLC. All rights reserved.
//

#import "WLTourMapViewController.h"

@implementation WLTourMapViewController

- (void)annotationClicked:(TSGGeotagAnnotation *)annotation
{
    d_main(^{
        WLTourGeotagViewController* vc = (WLTourGeotagViewController*)[WLTourGeotagViewController instantiateWithStoryboard];
        [vc loadGeotag:annotation.geotag withRoute:self.route shouldPlay:[TSGTriggerLogic lastTriggered].geotagId != annotation.geotag.geotagId];
        [self presentViewController:vc animated:YES completion:nil];
    });
}

- (void)close
{
    [super close];
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)connect
{
    [connectBarButton setHidden:YES];
    WLTourConnectViewController* vc = (WLTourConnectViewController*)[WLTourConnectViewController instantiateWithStoryboard];
    [vc loadOrg:[[TSGDatabase shared] loadOrgWithId:self.route.orgID] fromRoute:self.route];
    [self presentViewController:vc animated:YES completion:nil];
}

- (void)showAbout
{
    WLTourAboutViewController* vc = (WLTourAboutViewController*)[WLTourAboutViewController instantiateWithStoryboard];
    [vc loadRoute:self.route];
    [self presentViewController:vc animated:YES completion:nil];
}

+ (UIStoryboard*)localStoryboard
{
    NSBundle* localBundle = [NSBundle bundleForClass:WLTourMapViewController.class];
    if(hasNotch())
        return [UIStoryboard storyboardWithName:@"WLTourMapX" bundle:localBundle];
    return [UIStoryboard storyboardWithName:@"WLTourMap" bundle:localBundle];
}

@end
