//
//  WLPreloadViewController.m
//  WhiteLabelBase
//
//  Created by David Worth on 3/20/19.
//  Copyright © 2019 TravelStorysGPS, LLC. All rights reserved.
//

#import "WLPreloadViewController.h"

@implementation WLPreloadViewController

- (void)viewDidLoad
{
    NSString* logo = [Boilerplate getObjectValue:BoilerLogo];
    [self.logoView setImage:[UIImage imageNamed:logo inBundle:[NSBundle mainBundle] compatibleWithTraitCollection:nil]];
    
    UIColor* primary = ((UIColor*)[Boilerplate getObjectValue:BoilerColorPrimary]);
    //primary = [UIColor colorWithHex:@"00853d"];
    [self.progress setForegroundColor:primary];
    [self.progress setProgress:0];
    
    [self startPreload];
}

- (void)completeView
{
    if([[NSUserDefaults standardUserDefaults] boolForKey:@"config-permissions"])
    {
        WLDiscoveryViewController* discovery = (WLDiscoveryViewController*)[WLDiscoveryViewController instantiateWithStoryboard];
        [self presentViewController:discovery animated:YES completion:nil];
    }
    else
    {
        WLPermissionsViewController* permissions = (WLPermissionsViewController*)[WLPermissionsViewController instantiateWithStoryboard];
        [self presentViewController:permissions animated:YES completion:nil];
    }
}

+ (UIStoryboard*)localStoryboard
{
    NSBundle* localBundle = [NSBundle bundleForClass:WLPreloadViewController.class];
    return [UIStoryboard storyboardWithName:@"WLPreload" bundle:localBundle];
}

@end
