//
//  WLPreloadViewController.h
//  WhiteLabelBase
//
//  Created by David Worth on 3/20/19.
//  Copyright © 2019 TravelStorysGPS, LLC. All rights reserved.
//

#import <TravelStorysExtSDK/TravelStorysExtSDK.h>

#import "Boilerplate.h"
#import "WLPermissionsViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface WLPreloadViewController : TSGPreloadViewController

@end

NS_ASSUME_NONNULL_END
