//
//  WLTourGeotagViewController.m
//  WhiteLabelBase
//
//  Created by David Worth on 5/7/19.
//  Copyright © 2019 TravelStorysGPS, LLC. All rights reserved.
//

#import "WLTourGeotagViewController.h"

@interface WLTourGeotagViewController ()

@end

@implementation WLTourGeotagViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}



#pragma mark -
#pragma mark ITSGViewController

+ (UIStoryboard*)localStoryboard
{
    NSBundle* localBundle = [NSBundle bundleForClass:WLTourGeotagViewController.class];
    if(hasNotch())
        return [UIStoryboard storyboardWithName:@"WLTourGeotagX" bundle:localBundle];
    return [UIStoryboard storyboardWithName:@"WLTourGeotag" bundle:localBundle];
}

@end
