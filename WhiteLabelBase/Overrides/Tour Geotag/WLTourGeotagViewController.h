//
//  WLTourGeotagViewController.h
//  WhiteLabelBase
//
//  Created by David Worth on 5/7/19.
//  Copyright © 2019 TravelStorysGPS, LLC. All rights reserved.
//

#import <TravelStorysExtSDK/TravelStorysExtSDK.h>

NS_ASSUME_NONNULL_BEGIN

@interface WLTourGeotagViewController : TSGTourGeotagViewController

@end

NS_ASSUME_NONNULL_END
