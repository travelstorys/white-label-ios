//
//  WLTourAboutViewController.m
//  WhiteLabelBase
//
//  Created by David Worth on 5/16/19.
//  Copyright © 2019 TravelStorysGPS, LLC. All rights reserved.
//

#import "WLTourAboutViewController.h"

@interface WLTourAboutViewController ()

@end

@implementation WLTourAboutViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)close
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

+ (UIStoryboard*)localStoryboard
{
    NSBundle* localBundle = [NSBundle bundleForClass:self.class];
    if(hasNotch())
        return [UIStoryboard storyboardWithName:@"WLTourAboutX" bundle:localBundle];
    return [UIStoryboard storyboardWithName:@"WLTourAbout" bundle:localBundle];
}

@end
