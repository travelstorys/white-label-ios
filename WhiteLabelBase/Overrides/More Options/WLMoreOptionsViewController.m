//
//  WLMoreOptionsViewController.m
//  WhiteLabelBase
//
//  Created by David Worth on 5/15/19.
//  Copyright © 2019 TravelStorysGPS, LLC. All rights reserved.
//

#import "WLMoreOptionsViewController.h"

@interface WLMoreOptionsViewController ()

@end

@implementation WLMoreOptionsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)close
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)getPreviewControllerForRoute:(TSGERO *)route
{
    WLPreviewViewController* preview = (WLPreviewViewController*)[WLPreviewViewController instantiateWithStoryboard];
    [preview loadRoute:route];
    [self presentViewController:preview animated:YES completion:nil];
}

#pragma mark -
#pragma mark ITSGViewController

+ (UIStoryboard*)localStoryboard
{
    NSBundle* localBundle = [NSBundle bundleForClass:WLMoreOptionsViewController.class];
    if(hasNotch())
        return [UIStoryboard storyboardWithName:@"WLMoreOptionsX" bundle:localBundle];
    return [UIStoryboard storyboardWithName:@"WLMoreOptions" bundle:localBundle];
}

@end
