//
//  WLMoreOptionsViewController.h
//  WhiteLabelBase
//
//  Created by David Worth on 5/15/19.
//  Copyright © 2019 TravelStorysGPS, LLC. All rights reserved.
//

#import <TravelStorysExtSDK/TravelStorysExtSDK.h>

#import "WLPreviewViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface WLMoreOptionsViewController : TSGMoreOptionsViewController

@end

NS_ASSUME_NONNULL_END
