//
//  WLLaunchViewController.m
//  WhiteLabelBase
//
//  Created by David Worth on 3/19/19.
//  Copyright © 2019 TravelStorysGPS, LLC. All rights reserved.
//

#import "WLLaunchViewController.h"

@interface WLLaunchViewController ()

@end

@implementation WLLaunchViewController

BOOL darkMode = NO;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //Load from boilerplate
    //Load logo image
    NSString* logoImageName = (NSString*)[Boilerplate getObjectValue:BoilerLaunchLogo];
    UIImage* logo = [UIImage imageNamed:logoImageName inBundle:[NSBundle mainBundle] compatibleWithTraitCollection:nil];
    [self.logoView setImage: logo];
    
    //Load title
    NSString* title = (NSString*)[Boilerplate getObjectValue:BoilerLaunchTitle];
    [self.titleLabel setText:title];
    
    //Load subtitle
    NSString* subtitle = (NSString*)[Boilerplate getObjectValue:BoilerLaunchSubtitle];
    [self.subtitleLabel setText:subtitle];
    
    //Load dark mode
    if((BOOL)[Boilerplate getIntegerValue:BoilerLaunchDarkMode])
    {
        [self.view setBackgroundColor:[UIColor colorWithWhite:0.05 alpha:1]];
        [self.titleLabel setTextColor:[UIColor colorWithWhite:1 alpha:1]];
        [self.subtitleLabel setTextColor:[UIColor colorWithWhite:0.8 alpha:1]];
        darkMode = YES;
        [self setNeedsStatusBarAppearanceUpdate];
    }
    
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    [self proceed];
}

- (void)completeView
{
    [super completeView];
    [self proceed];
    //[[[UIAlertView alloc] initWithTitle:@"Beta Build" message:@"We are still waiting on your tours to be completed, so premium features are unavailable in this build. All customizations have been put into place, however.\n\nThis is a beta build of the software, meaning most features are present at this point, but you may still encounter some missing features and minor bugs.\n\nTHIS SOFTWARE IS NOT INTENDED FOR DISTRIBUTION AND MAY NOT BE SHARED.\n\nFor full terms and conditions, visit travelstorys.com/terms-of-service." delegate:self cancelButtonTitle:@"I understand and agree" otherButtonTitles: nil, nil] show];
}

- (void)proceed
{
    WLPreloadViewController* preload = (WLPreloadViewController*)[WLPreloadViewController instantiateWithStoryboard];
    [self presentViewController:preload animated:YES completion:nil];
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return (darkMode) ? UIStatusBarStyleLightContent : UIStatusBarStyleDefault;
}

+ (UIStoryboard*)localStoryboard
{
    NSBundle* localBundle = [NSBundle bundleForClass:WLLaunchViewController.class];
    return [UIStoryboard storyboardWithName:@"WLLaunch" bundle:localBundle];
}

@end
