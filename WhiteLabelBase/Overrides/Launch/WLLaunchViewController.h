//
//  WLLaunchViewController.h
//  WhiteLabelBase
//
//  Created by David Worth on 3/19/19.
//  Copyright © 2019 TravelStorysGPS, LLC. All rights reserved.
//

#import <TravelStorysExtSDK/TravelStorysExtSDK.h>

#import "Boilerplate.h"
#import "WLPreloadViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface WLLaunchViewController : TSGLaunchViewController <UIAlertViewDelegate>

@end

NS_ASSUME_NONNULL_END
