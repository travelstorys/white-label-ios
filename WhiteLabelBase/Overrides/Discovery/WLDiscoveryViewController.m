//
//  WLDiscoveryViewController.m
//  WhiteLabelBase
//
//  Created by David Worth on 4/15/19.
//  Copyright © 2019 TravelStorysGPS, LLC. All rights reserved.
//

#import "WLDiscoveryViewController.h"

@interface WLDiscoveryViewController ()

@end

@implementation WLDiscoveryViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)mapSelect:(RouteButton *)sender
{
    WLPreviewViewController* preview = (WLPreviewViewController*)[WLPreviewViewController instantiateWithStoryboard];
    [preview loadRoute:sender.annotation.route];
    [self presentViewController:preview animated:YES completion:nil];
}

- (void)previewItem:(TSGDiscoveryListItem *)item
{
    WLPreviewViewController* preview = (WLPreviewViewController*)[WLPreviewViewController instantiateWithStoryboard];
    [preview loadRoute:item.route];
    [self presentViewController:preview animated:YES completion:nil];
}

- (void)playItem:(TSGDiscoveryListItem *)item
{
    //load full route
    TSGRoute* fullRoute = [[TSGDatabase shared] loadRouteForRouteWithKey:item.route.routeKey];
    
    WLTourMapViewController* tourmap = (WLTourMapViewController*)[WLTourMapViewController instantiateWithStoryboard];
    [tourmap view];
    int track = 0;
    if(fullRoute.tracks.count > 0)
        track = fullRoute.tracks[0].trackID;
    [tourmap prepRoute:fullRoute withTrackId:track];
    [tourmap setModalTransitionStyle:UIModalTransitionStyleFlipHorizontal];
    [self presentViewController:tourmap animated:YES completion:nil];
}

- (void)showMoreOptions
{
    WLMoreOptionsViewController* options = (WLMoreOptionsViewController*)[WLMoreOptionsViewController instantiateWithStoryboard];
    [self presentViewController:options animated:YES completion:nil];
}

+ (UIImage*)backgroundImage
{
    return [UIImage imageNamed:[Boilerplate getObjectValue:BoilerDiscoveryBackground] inBundle:[NSBundle mainBundle] compatibleWithTraitCollection:nil];
}

- (void)routeRequested:(TSGERO *)route
{
    WLPreviewViewController* preview = (WLPreviewViewController*)[WLPreviewViewController instantiateWithStoryboard];
    [preview loadRoute:route];
    [self presentViewController:preview animated:YES completion:nil];
}

+ (UIStoryboard*)localStoryboard
{
    NSBundle* localBundle = [NSBundle bundleForClass:WLDiscoveryViewController.class];
    if(hasNotch())
        return [UIStoryboard storyboardWithName:@"WLDiscoveryX" bundle:localBundle];
    return [UIStoryboard storyboardWithName:@"WLDiscovery" bundle:localBundle];
}

@end
