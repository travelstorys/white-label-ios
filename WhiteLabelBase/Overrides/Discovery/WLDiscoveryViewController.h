//
//  WLDiscoveryViewController.h
//  WhiteLabelBase
//
//  Created by David Worth on 4/15/19.
//  Copyright © 2019 TravelStorysGPS, LLC. All rights reserved.
//

#import <TravelStorysExtSDK/TravelStorysExtSDK.h>

#import "Boilerplate.h"
#import "WLMoreOptionsViewController.h"
#import "WLPreviewViewController.h"
#import "WLTourMapViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface WLDiscoveryViewController : TSGDiscoveryViewController

@end

NS_ASSUME_NONNULL_END
