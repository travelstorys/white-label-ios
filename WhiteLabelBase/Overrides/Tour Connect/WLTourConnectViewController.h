//
//  WLTourConnectViewController.h
//  WhiteLabelBase
//
//  Created by David Worth on 5/16/19.
//  Copyright © 2019 TravelStorysGPS, LLC. All rights reserved.
//

#import <TravelStorysExtSDK/TravelStorysExtSDK.h>

NS_ASSUME_NONNULL_BEGIN

@interface WLTourConnectViewController : TSGTourConnectViewController

@end

NS_ASSUME_NONNULL_END
