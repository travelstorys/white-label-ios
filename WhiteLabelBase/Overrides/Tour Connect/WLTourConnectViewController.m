//
//  WLTourConnectViewController.m
//  WhiteLabelBase
//
//  Created by David Worth on 5/16/19.
//  Copyright © 2019 TravelStorysGPS, LLC. All rights reserved.
//

#import "WLTourConnectViewController.h"

@interface WLTourConnectViewController ()

@end

@implementation WLTourConnectViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)close
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark -
#pragma mark ITSGViewController

+ (UIStoryboard*)localStoryboard
{
    NSBundle* localBundle = [NSBundle bundleForClass:WLTourConnectViewController.class];
    if(hasNotch())
        return [UIStoryboard storyboardWithName:@"WLTourConnectX" bundle:localBundle];
    return [UIStoryboard storyboardWithName:@"WLTourConnect" bundle:localBundle];
}

@end
