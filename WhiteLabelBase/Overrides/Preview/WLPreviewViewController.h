//
//  WLPreviewViewController.h
//  WhiteLabelBase
//
//  Created by David Worth on 5/14/19.
//  Copyright © 2019 TravelStorysGPS, LLC. All rights reserved.
//

#import <TravelStorysExtSDK/TravelStorysExtSDK.h>

#import "Boilerplate.h"
#import "WLTourMapViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface WLPreviewViewController : TSGPreviewViewController <ITSGViewControllerDelegate>

@end

NS_ASSUME_NONNULL_END
