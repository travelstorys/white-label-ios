//
//  WLPreviewViewController.m
//  WhiteLabelBase
//
//  Created by David Worth on 5/14/19.
//  Copyright © 2019 TravelStorysGPS, LLC. All rights reserved.
//

#import "WLPreviewViewController.h"

@interface WLPreviewViewController ()

@end

@implementation WLPreviewViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)playAction
{
    TSGRoute* fullRoute = [[TSGDatabase shared] loadRouteForRouteWithKey:self.route.routeKey];
    
    if(fullRoute.tracks.count > 1)
    {
        UIViewController* vc = [TSGTrackSelectViewController instantiateWithStoryboard];
        TSGTrackSelectViewController* tsvc = (TSGTrackSelectViewController*)vc;
        [tsvc setDelegate:self];
        [tsvc view];
        [tsvc loadRoute:self.route];
        [self presentViewController:tsvc animated:YES completion:nil];
    }
    else
    {
        WLTourMapViewController* tourmap = (WLTourMapViewController*)[WLTourMapViewController instantiateWithStoryboard];
        [tourmap view];
        int track = 0;
        if(fullRoute.tracks.count > 0)
            track = fullRoute.tracks[0].trackID;
        [tourmap prepRoute:fullRoute withTrackId:track];
        [tourmap setModalTransitionStyle:UIModalTransitionStyleFlipHorizontal];
        [self presentViewController:tourmap animated:YES completion:nil];
    }
}

- (void)close
{
    [super close];
    [self dismissViewControllerAnimated:YES completion:nil];
    [NSObject cancelPreviousPerformRequestsWithTarget:self];
}

+ (UIStoryboard*)localStoryboard
{
    NSBundle* localBundle = [NSBundle bundleForClass:self.class];
    if(hasNotch())
        return [UIStoryboard storyboardWithName:@"WLPreviewX" bundle:localBundle];
    return [UIStoryboard storyboardWithName:@"WLPreview" bundle:localBundle];
}

#pragma mark -
#pragma mark ITSGViewControllerDelegate

- (void)requestResponderView:(NSDictionary *)info
{
    if([info[kITSGExpectedResponder] unsignedIntValue] == ResponderTourMap)
    {
        TSGTrack* track = info[@"track"];
        TSGRoute* route = info[@"route"];
        
        [self.presentedViewController dismissViewControllerAnimated:YES completion:^{
            WLTourMapViewController* tourmap = (WLTourMapViewController*)[WLTourMapViewController instantiateWithStoryboard];
            [tourmap view];
            int t = track.trackID;
            [tourmap prepRoute:route withTrackId:t];
            [tourmap setModalTransitionStyle:UIModalTransitionStyleFlipHorizontal];
            [self presentViewController:tourmap animated:YES completion:nil];
        }];
    }
}

@end
