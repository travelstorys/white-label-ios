//
//  WLPermissionsViewController.m
//  WhiteLabelBase
//
//  Created by David Worth on 4/12/19.
//  Copyright © 2019 TravelStorysGPS, LLC. All rights reserved.
//

#import "WLPermissionsViewController.h"

@interface WLPermissionsViewController ()

@end

@implementation WLPermissionsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

#pragma mark -
#pragma mark Boilerplate Overrides

+ (UIImage*)imageForState:(TSGPermissionState)state
{
    switch(state)
    {
        case PermissionWelcome:
        default:
            return [UIImage imageNamed:[Boilerplate getObjectValue:BoilerLogoTrans] inBundle:[NSBundle mainBundle] compatibleWithTraitCollection:nil];
        case PermissionLocation:
        case PermissionNotifications:
            return [super imageForState:state];
    }
}

+ (UIColor*)colorForState:(TSGPermissionState)state
{
    //Get boiler value
    NSArray* permissions = [Boilerplate getObjectValue:BoilerPermissions];
    NSArray* stateValue = @[@(BoilerColorPrimary), @(BoilerColorSecondary), @(BoilerColorTertiary), @(BoilerColorNeutral), @(BoilerColorNeutral2)];
    
    switch(state)
    {
        case PermissionWelcome:
            return [Boilerplate getObjectValue:([(stateValue[[permissions[0] intValue]]) intValue])];
        case PermissionLocation:
            return [Boilerplate getObjectValue:([(stateValue[[permissions[1] intValue]]) intValue])];
        case PermissionNotifications:
        default:
            return [Boilerplate getObjectValue:([(stateValue[[permissions[2] intValue]]) intValue])];
    }
}

+ (NSString*)titleForState:(TSGPermissionState)state
{
    switch(state)
    {
        case PermissionWelcome:
        {
            NSDictionary* welcome = [Boilerplate getObjectValue:BoilerPermissionsWelcome];
            return welcome[@"title"];
            break;
        }
        default:
            return [super titleForState:state];
            break;
    }
}

+ (NSString*)bodyForState:(TSGPermissionState)state
{
    switch(state)
    {
        case PermissionWelcome:
        {
            NSDictionary* welcome = [Boilerplate getObjectValue:BoilerPermissionsWelcome];
            return welcome[@"body"];
            break;
        }
        default:
            return [super bodyForState:state];
            break;
    }
}

+ (NSString*)firstButtonForState:(TSGPermissionState)state
{
    switch(state)
    {
        case PermissionWelcome:
        {
            NSDictionary* welcome = [Boilerplate getObjectValue:BoilerPermissionsWelcome];
            return welcome[@"firstButton"];
            break;
        }
        default:
        {
            return [super firstButtonForState:state];
            break;
        }
    }
}

+ (NSString*)secondButtonForState:(TSGPermissionState)state
{
    switch(state)
    {
        default:
        {
            return [super secondButtonForState:state];
            break;
        }
    }
}

- (void)completeView
{
    d_main(^{
        [super completeView];
        WLDiscoveryViewController* discovery = (WLDiscoveryViewController*)[WLDiscoveryViewController instantiateWithStoryboard];
        [self presentViewController:discovery animated:YES completion:nil];
    });
}

#pragma mark -
#pragma mark ITSGViewControllerDelegate

+ (UIStoryboard*)localStoryboard
{
    NSBundle* localBundle = [NSBundle bundleForClass:WLPermissionsViewController.class];
    return [UIStoryboard storyboardWithName:@"WLPermissions" bundle:localBundle];
}

@end
