//
//  WLPermissionsViewController.h
//  WhiteLabelBase
//
//  Created by David Worth on 4/12/19.
//  Copyright © 2019 TravelStorysGPS, LLC. All rights reserved.
//

#import <TravelStorysExtSDK/TravelStorysExtSDK.h>

#import "Boilerplate.h"
#import "WLDiscoveryViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface WLPermissionsViewController : TSGPermissionsViewController

@end

NS_ASSUME_NONNULL_END
