//
//  TSGTourConnectViewController.h
//  TravelStorysExtSDK
//
//  Created by David Worth on 5/16/19.
//  Copyright © 2019 TravelStorysGPS, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "TSGBoilerViewSupport.h"
#import "TSGDatabase.h"
#import "TSGDataManager.h"
#import "ITSGViewController.h"
#import "TSGOrg.h"
#import "TSGShorthand.h"
#import "TSGSocialManager.h"

#import "TSGAnalyticsManager.h"

NS_ASSUME_NONNULL_BEGIN

@interface TSGTourConnectViewController : UIViewController <ITSGViewController>
{
    IBOutlet UIImageView* backgroundImageView;
    
    IBOutlet UIButton* titleBackButton;
    IBOutlet UIButton* titleShareButton;
    
    IBOutlet UILabel* connectTitleLabel;
    IBOutlet UILabel* connectSubtitleLabel;
    
    IBOutlet UIImageView* connectImageBackground;
    IBOutlet UIImageView* connectLogo;
    
    IBOutlet UITextView* connectMessageView;
    IBOutlet UIButton* connectTargetButton;
    IBOutlet UIImageView* connectTargetIcon;
}

@property (nonatomic) id<ITSGViewControllerDelegate> delegate;

@property (nonatomic, readonly) TSGOrg* org;

- (void)loadOrg:(TSGOrg*)org fromRoute:(TSGRoute*)route;
- (void)close;

@end

NS_ASSUME_NONNULL_END
