//
//  ITSGViewController.h
//  TravelStorysExtSDK
//
//  Created by David Worth on 3/4/19.
//  Copyright © 2019 TravelStorysGPS, LLC. All rights reserved.
//

#ifndef ITSGViewController_h
#define ITSGViewController_h

#define kITSGExpectedResponder @"ExpectedResponder"

typedef enum TSGResponderSet : UInt8 {
    ResponderMoreOptions,
    ResponderPreview,
    ResponderTourAbout,
    ResponderTourConnect,
    ResponderTourMap,
    ResponderTourGeotag
} TSGResponderSet;

@protocol ITSGViewControllerDelegate <NSObject>

@optional
/**
 * @brief
 * called when the view has reached the end of
 * its sequence and the next view should be presented.
 */
- (void)requestNextView:(id)sender;
- (void)requestResponderView:(NSDictionary*)info;

@end

@protocol ITSGViewController <NSObject>

+ (UIStoryboard*)localStoryboard;
+ (UIViewController*)instantiateWithStoryboard;

@end

#endif /* ITSGViewController_h */
