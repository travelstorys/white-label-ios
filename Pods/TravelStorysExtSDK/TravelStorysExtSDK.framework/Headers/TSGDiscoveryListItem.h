//
//  TSGDiscoveryListItem.h
//  TravelStorysExtSDK
//
//  Created by David Worth on 4/16/19.
//  Copyright © 2019 TravelStorysGPS, LLC. All rights reserved.
//

#import <CoreText/CoreText.h>
#import <UIKit/UIKit.h>

#import "TSGBoilerViewSupport.h"
#import "TSGDatabase.h"
#import "TSGDataManager.h"
#import "TSGDownloadManager.h"
#import "TSGERO.h"
#import "TSGShorthand.h"
#import "TSGFlatProgressView.h"

#import "TSGCircleProgressView.h"

#import "TSGPurchaseViewController.h"

#import "TSGNSString+Hash.h"

NS_ASSUME_NONNULL_BEGIN

@class TSGDiscoveryListItem;
@protocol TSGDiscoveryListItemDelegate

- (void)previewItem:(TSGDiscoveryListItem*)item;
- (void)playItem:(TSGDiscoveryListItem*)item;

@end

@interface TSGDiscoveryListItem : UIView <DownloadProgressDelegate, TSGPurchaseDelegate>
{
    IBOutlet UIImageView* imageView;
    IBOutlet UILabel* titleLabel;
    IBOutlet UILabel* subtitleLabel;
    IBOutlet UILabel* stateLabel;
    IBOutlet UILabel* distanceLabel;
    IBOutlet UIImageView* typeIcon;
    
    IBOutlet UIButton* coverButton;
    IBOutlet UIButton* actionButton;
    
    IBOutlet UILabel* comingSoonLabel;
    IBOutlet UILabel* updateRequiredLabel;
    
    TSGDownloadTaskGroup* taskGroup;
    
    IBOutlet UIActivityIndicatorView* activity;
    
    IBOutlet UIImageView* premiumBackground;
    IBOutlet UILabel* premiumLabel;
    
    IBOutlet UIVisualEffectView* downloadingView;
    IBOutlet UIProgressView* progressView;
    IBOutlet UILabel* downloadingTitleLabel;
    IBOutlet UILabel* downloadingSizeLabel;
}

@property (nonatomic) TSGERO* route;
@property (nonatomic) id<TSGDiscoveryListItemDelegate> delegate;

- (void)setDistance:(float)meters;
- (void)refresh;

@end

NS_ASSUME_NONNULL_END
