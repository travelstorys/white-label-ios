//
//  TSGDataManager.h
//  TravelStorysExtSDK
//
//  Created by David Worth on 3/26/19.
//  Copyright © 2019 TravelStorysGPS, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

#import "TSGNSString+Hash.h"
#import "Reachability.h"
#import "TSGShorthand.h"

NS_ASSUME_NONNULL_BEGIN

#define DataDirectoryConfig     @"DataConfig"
#define DataDirectoryRequired   @"DataRequired"
#define DataDirectoryRoute      @"DataRoute"
#define DataDirectoryTemp       @"DataTemp"

@interface TSGDataManager : NSObject

+ (NSString*)pathForDirectory:(NSString*)directory;
+ (NSString*)pathForDirectory:(NSString *)directory withName:(NSString*)name;
+ (NSString*)pathForDirectory:(NSString *)directory withName:(nullable NSString*)name withURL:(NSString *)url;

+ (BOOL)dataIsCachedWithDirectory:(NSString*)directory withName:(nullable NSString*)name withURL:(NSString*)url;
+ (void)saveData:(NSData *)data withDirectory:(NSString *)directory withName:(nullable NSString *)name withURL:(NSString *)url;
+ (NSData*)loadDataAtPath:(NSString*)path;
+ (NSData*)loadDataWithDirectory:(NSString*)directory withName:(nullable NSString*)name withURL:(NSString*)url;

+ (BOOL)checkHeadersForFile:(NSString*)directory withName:(nullable NSString*)name withURL:(NSString*)url;

+ (void)fetchDataWithDirectory:(NSString*)directory withName:(nullable NSString*)name withURL:(NSString*)url withCallback:(void(^)(NSData*))callback;
+ (void)preloadImageView:(UIImageView*)imageView withDirectory:(NSString*)directory withName:(nullable NSString*)name withURL:(NSString*)url;

@end

NS_ASSUME_NONNULL_END
