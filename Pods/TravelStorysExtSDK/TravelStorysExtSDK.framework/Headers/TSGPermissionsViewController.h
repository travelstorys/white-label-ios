//
//  TSGPermissionsViewController.h
//  TravelStorysExtSDK
//
//  Created by David Worth on 4/10/19.
//  Copyright © 2019 TravelStorysGPS, LLC. All rights reserved.
//

#import <CoreLocation/CoreLocation.h>
#import <NotificationCenter/NotificationCenter.h>
#import <UIKit/UIKit.h>
#import <UserNotifications/UserNotifications.h>

#import "ITSGViewController.h"

#import "TSGPermissionSlide.h"

#import "TSGUIColor+Hex.h"

#import "TSGAnalyticsManager.h"

NS_ASSUME_NONNULL_BEGIN

typedef enum TSGPermissionState : UInt8 {
    
    PermissionWelcome,
    PermissionLocation,
    PermissionNotifications
    
} TSGPermissionState;

@interface TSGPermissionsViewController : UIViewController <ITSGViewController, CLLocationManagerDelegate, UIAlertViewDelegate>
{
    IBOutlet UIPageControl* pageControl;
    
    TSGPermissionSlide* welcomeSlide;
    TSGPermissionSlide* locationSlide;
    TSGPermissionSlide* notificationSlide;
    
    TSGPermissionState currentState;
    
    CLLocationManager* locationManager;
}

@property (nonatomic) id<ITSGViewControllerDelegate> delegate;

+ (UIImage*)imageForState:(TSGPermissionState)state;
+ (UIColor*)colorForState:(TSGPermissionState)state;
+ (NSString*)titleForState:(TSGPermissionState)state;
+ (NSString*)bodyForState:(TSGPermissionState)state;
+ (NSString*)firstButtonForState:(TSGPermissionState)state;
+ (NSString*)secondButtonForState:(TSGPermissionState)state;

- (void)completeView;

@end

NS_ASSUME_NONNULL_END
