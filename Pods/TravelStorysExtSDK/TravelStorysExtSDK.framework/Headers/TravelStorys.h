//
//  TravelStorys.h
//  TravelStorysExtSDK
//
//  Created by David Worth on 2/22/19.
//  Copyright © 2019 TravelStorysGPS, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

typedef enum TravelStorysStatus : UInt8
{
    StatusConnected,
    StatusNotConnected
} TravelStorysStatus;

typedef enum TravelStorysLogin : UInt8
{
    LoggedInTravelStorys,
    LoggedInFacebook,
    LoggedInTemporary,
    NotLoggedIn
} TravelStorysLogin;

@interface TravelStorys : NSObject

+ (void)setAPIKey:(NSString*)apiKey;
+ (void)setAPISecret:(NSString*)apiSecret;

+ (void)connect:(void(^)(BOOL))callback;
+ (void)checkForUpdates:(void(^)(int))callback;

+ (void)refreshAllRoutes:(void(^)(NSArray<NSDictionary*>*))callback;
+ (NSDictionary*)getRoute:(NSString*)routeKey;
+ (NSDictionary*)getRoutes:(NSArray<NSString*>*)routeKeys;

+ (BOOL)loginWithEmail:(NSString*)email withPassword:(NSString*)password isFacebook:(BOOL)facebook;
+ (int)registerWithEmail:(NSString*)email withPassword:(NSString*)password isFacebook:(BOOL)facebook withFirstName:(NSString*)first withLastName:(NSString*)lastName;
+ (NSDictionary*)validateTemporaryID;
+ (BOOL)deviceIsAssigned;

+ (NSDictionary*)getProductInfo:(int)tourID;
+ (NSDictionary*)getPurchaseCode:(NSString*)code;

+ (TravelStorysLogin)getLoginStatus;
+ (void)logout;

+ (void)setFacebookEnabled:(BOOL)facebookEnabled;

@end

NS_ASSUME_NONNULL_END
