//
//  TSGGeotagImage.h
//  TravelStorysExtSDK
//
//  Created by David Worth on 3/25/19.
//  Copyright © 2019 TravelStorysGPS, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface TSGGeotagImage : NSObject

@property (nonatomic) int index;
@property (nonatomic) NSString* url;
@property (nonatomic) NSString* caption;

@end

NS_ASSUME_NONNULL_END
