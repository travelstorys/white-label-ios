//
//  TSGAudioControls.h
//  TravelStorysExtSDK
//
//  Created by David Worth on 9/13/19.
//  Copyright © 2019 TravelStorysGPS, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "TSGAudioManager.h"

NS_ASSUME_NONNULL_BEGIN

@interface TSGAudioControls : NSObject <TSGAudioDelegateListener>

+ (void)updateControls:(BOOL)enable;

@end

NS_ASSUME_NONNULL_END
