//
//  TSGTrackSelectViewController.h
//  TravelStorysExtSDK
//
//  Created by David Worth on 7/24/19.
//  Copyright © 2019 TravelStorysGPS, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "TSGBoilerViewSupport.h"
#import "TSGConstants.h"
#import "TSGDeepLinkHandler.h"
#import "ITSGViewController.h"
#import "TSGShorthand.h"

#import "TSGDatabase.h"
#import "TSGDataManager.h"

#import "TSGTrackSelectItem.h"

#import "TSGAnalyticsManager.h"

NS_ASSUME_NONNULL_BEGIN

@interface TSGTrackSelectViewController : UIViewController <ITSGViewController>
{
    IBOutlet UIImageView* backgroundImageView;
    IBOutlet UIButton* titleBackButton;
    IBOutlet UIScrollView* scrollView;
}

- (void)loadRoute:(TSGERO*)route;

@property (readonly) TSGRoute* route;
@property id<ITSGViewControllerDelegate> delegate;

@end

NS_ASSUME_NONNULL_END
