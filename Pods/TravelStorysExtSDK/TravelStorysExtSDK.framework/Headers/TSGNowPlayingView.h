//
//  TSGNowPlayingView.h
//  TravelStorysExtSDK
//
//  Created by David Worth on 5/5/19.
//  Copyright © 2019 TravelStorysGPS, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "TSGAudioManager.h"
#import "TSGSmallSlider.h"

NS_ASSUME_NONNULL_BEGIN

@class TSGNowPlayingView;

@protocol NowPlayingViewDelegate <TSGSmallSliderDelegate>

- (void)nowPlayingViewPressedPlay;
- (void)nowPlayingViewPressedPause;

@end

@interface TSGNowPlayingView : UIView
{
    IBOutlet TSGSmallSlider* slider;
    IBOutlet UILabel* titleLabel;
    IBOutlet UILabel* subtitleLabel;
    IBOutlet UIButton* playButton;
}

@property (nonatomic) id<NowPlayingViewDelegate> delegate;

- (IBAction)playPausePressed:(id)sender;

- (void)setProgress:(float)progress;
- (void)setTitleAndSubtitle:(NSString*)title subtitle:(NSString*)subtitle;
- (NSString*)getTitle;
- (NSString*)getSubtitle;
- (BOOL)isPaused;
- (void)updatePlayPause:(BOOL)pause;

@end

NS_ASSUME_NONNULL_END
