//
//  TSGERO.h
//  TravelStorysExtSDK
//
//  Created by David Worth on 3/27/19.
//  Copyright © 2019 TravelStorysGPS, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

#import "TSGConstants.h"

NS_ASSUME_NONNULL_BEGIN

/**
 *@brief Expanded TSGRoute Object is used for displaying basic information about
 * the route and is used in discovery and preview views.
 *
 */
@interface TSGERO : NSObject

/**
 *@brief Internal TSG TSGRoute ID
 *
 */
@property (nonatomic) int routeId;

/**
 *@brief Unique alphanumeric route key.
 *
 */
@property (nonatomic) NSString* routeKey;

/**
 *@brief TSG ID for the primary org on the route.
 *
 */
@property (nonatomic) int orgID;

/**
 *@brief Name of the primary org on the route.
 *
 */
@property (nonatomic) NSString* orgString;

/**
 *@brief Array of all orgs' IDs on the route
 *
 */
@property (nonatomic) NSArray<NSNumber*>* allOrgs;

/**
 *@brief TSG ID for the primary sponsor on the route.
 *
 */
@property (nonatomic) int sponsorID;

/**
 *@brief Name of the primary sponsor on the route.
 *
 */
@property (nonatomic) NSString* sponsorString;

/**
 *@brief Title of the route
 *
 */
@property (nonatomic) NSString* title;

/**
 *@brief Subtitle of the route
 *
 */
@property (nonatomic) NSString* subtitle;

/**
 *@brief State/Country where the route is located.
 *
 */
@property (nonatomic) NSString* state;

/**
 *@brief Transit type of the route.
 *@details See TSGConstants.h for acceptable values
 *
 */
@property (nonatomic) TSGRouteType type;

/**
 *@brief True if the route is active
 *
 */
@property (nonatomic) BOOL live;

/**
 *@brief Release status of the route.
 *@details See TSGConstants.h for acceptable values
 *
 */
@property (nonatomic) ReleaseState releaseState;

/**
 *@brief Minimum app/sdk version the route requires
 *@details For SDK usage, this is based on the SDK version.
 *
 */
@property (nonatomic) NSString* minVersion;

/**
 *@brief Estimated length of the route, in minutes.
 *
 */
@property (nonatomic) int tourLength;

/**
 *@brief Formatted length of the route with time descriptors.
 *
 */
@property (nonatomic) NSString* lengthString;

/**
 *@brief Summary of the route.
 *
 */
@property (nonatomic) NSString* summary;

/**
 *@brief True if the route is premium
 *
 */
@property (nonatomic) BOOL paid;

/**
 *@brief Passcode used to enter the route without restriction.
 *
 */
@property (nonatomic) NSString* passcode;

/**
 *@brief URL of the route's discovery view image.
 *
 */
@property (nonatomic) NSString* discoveryImage;

/**
 *@brief Approximate center point of the route.
 *
 */
@property (nonatomic) CLLocationCoordinate2D coordinate;

/**
 *@brief Distance to the center point of the route, in meters.
 *
 */
@property (nonatomic) float distance;

/**
 *@brief Additional information about the route.
 *
 */
@property (nonatomic) NSString* flags;

/**
 *@brief Route's assets
 *
 */
@property (nonatomic) NSDictionary* assets;

@end

NS_ASSUME_NONNULL_END
