//
//  TSGLaunchViewController.h
//  TravelStorysExtSDK
//
//  Created by David Worth on 3/4/19.
//  Copyright © 2019 TravelStorysGPS, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <TravelStorysExtSDK/TSGAnimPoweredByView.h>

#import "ITSGViewController.h"

#import "TSGPreloadViewController.h"

#import "TSGAnalyticsManager.h"


NS_ASSUME_NONNULL_BEGIN

@interface TSGLaunchViewController : UIViewController<ITSGViewController>

@property (nonatomic, assign) IBOutlet TSGAnimPoweredByView* animView;
@property (nonatomic, assign) IBOutlet UIImageView* logoView;
@property (nonatomic, assign) IBOutlet UILabel* titleLabel;
@property (nonatomic, assign) IBOutlet UILabel* subtitleLabel;
@property (nonatomic, assign) IBOutlet UILabel* versionLabel;

@property (nonatomic, assign) id<ITSGViewControllerDelegate> delegate;

- (void)completeView;

@end

NS_ASSUME_NONNULL_END
