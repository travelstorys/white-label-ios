//
//  TSGMarkdownTextView.h
//  TravelStorys
//
//  Created by David Worth on 7/3/17.
//  Copyright © 2017 TravelStorysGPS. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "TSGUIColor+Hex.h"

@interface TSGMarkdownTextView : UITextView <UITextViewDelegate>

- (void)setMarkdownText:(NSString*)text;

@end
