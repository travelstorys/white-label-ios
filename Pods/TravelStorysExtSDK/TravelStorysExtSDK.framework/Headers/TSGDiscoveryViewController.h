//
//  TSGDiscoveryViewController.h
//  TravelStorysExtSDK
//
//  Created by David Worth on 4/14/19.
//  Copyright © 2019 TravelStorysGPS, LLC. All rights reserved.
//

#import <CoreLocation/CoreLocation.h>
#import <MapKit/MapKit.h>
#import <UIKit/UIKit.h>

#import "TravelStorys.h"

#import "TSGConstants.h"
#import "TSGDatabase.h"
#import "TSGDeepLinkHandler.h"
#import "TSGGeotagAnnotation.h"
#import "ITSGViewController.h"
#import "ScoreSearch.h"
#import "TSGShorthand.h"

#import "TSGCircleActivityView.h"
#import "TSGDiscoveryListItem.h"

#import "TSGLoginViewController.h"
#import "TSGModalWebViewController.h"

#import "TSGAnalyticsManager.h"

NS_ASSUME_NONNULL_BEGIN

@interface TSGDiscoveryViewController : UIViewController <CLLocationManagerDelegate, TSGDiscoveryListItemDelegate, ITSGViewController, MKMapViewDelegate, UITextFieldDelegate, UIScrollViewDelegate, TSGLoginDelegate, TSGDeepLinkResponder, TSGSyncDelegate>
{
    IBOutlet UIImageView* backgroundImageView;
    
    IBOutlet UIView* titleBackingView;
    IBOutlet UIButton* titleMapButton;
    IBOutlet UIButton* titleAccountButton;
    IBOutlet UIButton* titleSearchButton;
    IBOutlet UIButton* titleMoreButton;
    
    IBOutlet UIView* searchPanel;
    IBOutlet UITextField* searchField;
    IBOutlet NSLayoutConstraint* topSearchPanelConstraint;
    IBOutlet NSLayoutConstraint* bottomScrollViewConstraint;
    
    IBOutlet UIScrollView* scrollView;
    
    IBOutlet TSGCircleActivityView* activityView;
    
    IBOutlet MKMapView* mapView;
    
    CLLocationManager* locationManager;
    NSMutableArray<TSGERO*>* eros;
}

@property (nonatomic) id<ITSGViewControllerDelegate> delegate;

+ (UIImage*)backgroundImage;
- (void)mapSelect:(RouteButton*)sender;
- (void)showMoreOptions;

@end

NS_ASSUME_NONNULL_END
