//
//  TSGAnalyticsManager.h
//  TravelStorysExtSDK
//
//  Created by David Worth on 10/18/19.
//  Copyright © 2019 TravelStorysGPS, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

#import "TSGShorthand.h"

NS_ASSUME_NONNULL_BEGIN

@protocol TSGAnalyticsManagerDelegate <NSObject>

- (void)sendScreen:(NSString *)screenName;
- (void)report:(NSString*)category withAction:(NSString *)action withLabel:(NSString *)label withValue:(double)value;

@end

@interface TSGAnalyticsManager : NSObject

+ (void)sendScreen:(NSString*)screenName;
+ (void)report:(NSString*)category
    withAction:(NSString *)action
     withLabel:(NSString *)label
     withValue:(double)value;

@end

NS_ASSUME_NONNULL_END
