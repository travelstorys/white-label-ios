//
//  SyncViewController.h
//  TravelStorysExtSDK
//
//  Created by David Worth on 7/16/19.
//  Copyright © 2019 TravelStorysGPS, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "ITSGViewController.h"
#import "TSGShorthand.h"

#import "TSGCircleActivityView.h"

#import "TSGAnalyticsManager.h"

NS_ASSUME_NONNULL_BEGIN

@protocol TSGSyncDelegate <NSObject>

- (void)syncComplete;

@end

@interface TSGSyncViewController : UIViewController <ITSGViewController>
{
    IBOutlet TSGCircleActivityView* activity;
}

@property (nonatomic) id<TSGSyncDelegate> delegate;

@end

NS_ASSUME_NONNULL_END
