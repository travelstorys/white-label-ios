//
//  TSGLoginViewController.h
//  TravelStorysExtSDK
//
//  Created by David Worth on 6/18/19.
//  Copyright © 2019 TravelStorysGPS, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <TravelStorysExtSDK/TravelStorys.h>

#import "TSGConstants.h"
#import "ITSGViewController.h"
#import "TSGShorthand.h"

#import "TSGSyncViewController.h"

#import "TSGAnalyticsManager.h"

NS_ASSUME_NONNULL_BEGIN

@protocol TSGLoginDelegate <NSObject>

- (void)loginComplete:(TravelStorysLogin)status;
- (void)loginSkipped;
- (void)loginClosed;

@end

@protocol TSGLoginFacebookDelegate <NSObject>

- (void)facebookLogin:(UIViewController*)sender withCallback:(void (^)(BOOL, NSString *, NSString *))callback;

@end

@interface TSGLoginViewController : UIViewController <ITSGViewController, UITextFieldDelegate, TSGSyncDelegate>
{
    IBOutlet UIImageView* backgroundImageView;
    
    IBOutlet UIButton* closeButton;
    
    IBOutlet UIView* promptView;
    IBOutlet UIView* loginView;
    IBOutlet UIView* registerView;
    
    IBOutlet UIButton* promptLoginButton;
    IBOutlet UIButton* promptRegisterButton;
    IBOutlet UIButton* promptFacebookButton;
    IBOutlet UIImageView* promptFacebookIcon;
    IBOutlet UIButton* promptSkipButton;
    IBOutlet UIImageView* promptSkipIcon;
    
    IBOutlet UIButton* loginBackButton;
    IBOutlet UIButton* loginSubmitButton;
    IBOutlet UIButton* loginForgotButton;
    IBOutlet UITextField* loginEmailField;
    IBOutlet UITextField* loginPassField;
    
    IBOutlet UIButton* registerBackButton;
    IBOutlet UIButton* registerSubmitButton;
    IBOutlet UITextField* registerFirstField;
    IBOutlet UITextField* registerLastField;
    IBOutlet UITextField* registerEmailField;
    IBOutlet UITextField* registerPassField;
    IBOutlet UITextField* registerConfirmField;
}

@property (nonatomic) id<TSGLoginDelegate> delegate;

@end

NS_ASSUME_NONNULL_END
