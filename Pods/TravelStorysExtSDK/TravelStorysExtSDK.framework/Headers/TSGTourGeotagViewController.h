//
//  TSGTourGeotagViewController.h
//  TravelStorysExtSDK
//
//  Created by David Worth on 5/6/19.
//  Copyright © 2019 TravelStorysGPS, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "ITSGViewController.h"

#import "TSGAudioManager.h"
#import "TSGDatabase.h"
#import "TSGDataManager.h"
#import "TSGShorthand.h"
#import "TSGSocialManager.h"

#import "TSGBoilerViewSupport.h"
#import "TSGGeotag.h"
#import "TSGMarkdownTextView.h"
#import "TSGNowPlayingView.h"
#import "TSGPagingView.h"
#import "TSGRoute.h"

#import "TSGAnalyticsManager.h"

NS_ASSUME_NONNULL_BEGIN

@interface TSGTourGeotagViewController : UIViewController <ITSGViewController, TSGAudioManagerSubscriber, NowPlayingViewDelegate>
{
    IBOutlet UILabel* titleLabel;
    IBOutlet UIButton* titleBackButton;
    IBOutlet UIButton* titleVideoButton;
    IBOutlet UIButton* titleShareButton;
    
    IBOutlet UIImageView* backgroundImageView;
    
    IBOutlet UIImageView* firstImageView;
    IBOutlet UIImageView* secondImageView;
    
    IBOutlet TSGPagingView* pageView;
    IBOutlet TSGMarkdownTextView* bodyTextView;
    IBOutlet TSGNowPlayingView* nowPlayingView;
    
    IBOutlet UIView* captionView;
    IBOutlet UILabel* captionLabel;
    IBOutlet NSLayoutConstraint* captionBottomConstraint;
    
    IBOutlet UIView* gestureView;
    
    IBOutlet UIView* titleBar;
}

- (void)loadGeotag:(TSGGeotag*)geotag withRoute:(TSGRoute*)route shouldPlay:(BOOL)shouldPlay;

- (IBAction)toggleCaption:(id)sender;

@property (nonatomic) TSGGeotag* geotag;
@property (nonatomic) TSGRoute* route;

@property (nonatomic) BOOL usePreviewImages;
@property (nonatomic) BOOL useOrgImage;

@end

NS_ASSUME_NONNULL_END
