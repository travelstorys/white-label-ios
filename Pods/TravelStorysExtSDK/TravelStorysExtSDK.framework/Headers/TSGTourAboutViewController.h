//
//  TSGTourAboutViewController.h
//  TravelStorysExtSDK
//
//  Created by David Worth on 5/16/19.
//  Copyright © 2019 TravelStorysGPS, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "TSGBoilerViewSupport.h"
#import "TSGDatabase.h"
#import "TSGDataManager.h"
#import "ITSGViewController.h"
#import "TSGShorthand.h"
#import "TSGSocialManager.h"

#import "TSGOrg.h"
#import "TSGOrgListItem.h"
#import "TSGOrgListView.h"
#import "TSGRoute.h"

#import "TSGAnalyticsManager.h"

NS_ASSUME_NONNULL_BEGIN

@interface TSGTourAboutViewController : UIViewController <ITSGViewController>
{
    IBOutlet UIImageView* backgroundImageView;
    
    IBOutlet UIButton* titleBackButton;
    IBOutlet UIButton* titleShareButton;
    
    IBOutlet TSGOrgListView* listView;
}

@property (nonatomic, readonly) TSGRoute* route;
@property (nonatomic) id<ITSGViewControllerDelegate> delegate;

- (void)loadRoute:(TSGRoute*)route;
- (void)close;

@end

NS_ASSUME_NONNULL_END
