//
//  TSGBackgroundManager.h
//  TravelStorysExtSDK
//
//  Created by David Worth on 10/11/19.
//  Copyright © 2019 TravelStorysGPS, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "TSGDataManager.h"
#import "TSGDownloadManager.h"

NS_ASSUME_NONNULL_BEGIN

@protocol TSGBackgroundObserver <NSObject>

- (void)backgroundQueueFinished;

@optional
- (void)backgroundTaskComplete:(TSGDownloadTask*)task;

@end

@interface TSGBackgroundManager : NSObject <DownloadProgressDelegate>

+ (TSGBackgroundManager*)shared;

- (void)addObserver:(id<TSGBackgroundObserver>)observer;
- (void)removeObserver:(id<TSGBackgroundObserver>)observer;

- (void)addToQueue:(NSArray<NSString*>*)urls;
- (void)processQueue;

- (BOOL)hasActiveQueue;

@end

NS_ASSUME_NONNULL_END
