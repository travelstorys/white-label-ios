//
//  TSGOrgListView.h
//  TravelStorysExtSDK
//
//  Created by David Worth on 5/16/19.
//  Copyright © 2019 TravelStorysGPS, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "TSGOrg.h"
#import "TSGOrgListItem.h"

NS_ASSUME_NONNULL_BEGIN

@interface TSGOrgListView : UIScrollView

- (void)addOrg:(TSGOrg*)org;
- (void)addSeparator:(NSString*)title;

@end

NS_ASSUME_NONNULL_END
