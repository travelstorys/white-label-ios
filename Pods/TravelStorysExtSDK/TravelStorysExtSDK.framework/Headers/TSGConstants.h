//
//  TSGConstants.h
//  TravelStorysExtSDK
//
//  Created by David Worth on 3/25/19.
//  Copyright © 2019 TravelStorysGPS, LLC. All rights reserved.
//

#ifndef Constants_h
#define Constants_h

#define kNotificationRefreshFromNotification @"TSG_DISCOVERY_REFRESH"
#define kNotificationReturnNone @"TS_NOTIFY_RETURN_NONE"
#define kNotificationPromoCode @"TS_NOTIFY_PROMO_CODE"


typedef enum TSGRouteType : UInt8
{
    RouteUnknown = -1,
    RouteCycle = 0,
    RouteDrive = 1,
    RouteDriveWalk = 2,
    RoutePaddle = 3,
    RouteTrain = 4,
    RouteWalk = 5,
    RouteWalkCycle = 6
    
} TSGRouteType;

typedef enum TSGOrganizationType : UInt8
{
    OrganizationUnknown = -1,
    OrganizationAgency = 0,
    OrganizationCommercial = 1,
    OrganizationCorporateSponsor = 2,
    OrganizationNonProfit = 3,
    OrganizationNonProfitSponsor = 4,
    OrganizationStateAgency = 5
} TSGOrganizationType;

typedef enum TSGSponsorType : UInt8
{
    SponsorUnknown = -1,
    SponsorGalleries = 0,
    SponsorRestaurants = 1
} TSGSponsorType;

typedef enum ReleaseState : UInt8
{
    ReleaseUnknown = -1,
    ReleaseTest = 0,
    ReleaseAlpha = 1,
    ReleaseBeta = 2,
    ReleaseLive = 3
} ReleaseState;

typedef enum TSGGeotagPin : NSInteger
{
    PinFlagRed = 0,
    PinDefaultGreen = 1,
    PinDefaultLime = 2,
    PinDefaultOrange = 3,
    PinDefaultTeal = 4,
    PinDefaultTurquoise = 5,
    
    PinGreenBird = 6,
    PinGreenCrab = 7,
    PinGreenDolphin = 8,
    PinGreenFish = 9,
    PinGreenPalmtree = 10,
    PinGreenPelican = 11,
    PinGreenShell = 12,
    PinGreenTurtle = 13,
    PinGreenTurtlePatrol = 14,
    
    PinRedBed = 15,
    PinRedCamera = 16,
    PinRedForkKnife = 17,
    PinRedHikingBoot = 18,
    PinRedInfo = 19,
    PinRedMountain = 20,
    PinRedPicnic = 21,
    PinRedShoppingBag = 22,
    PinRedTent = 23,
    
    PinCustom = -1
} TSGGeotagPin;

typedef enum TSGGeotagTriggerMode : UInt8
{
    TriggerRadial = 0,
    TriggerPolygon = 1,
    TriggerBeacon = 2
} TSGGeotagTriggerMode;

#endif /* Constants_h */
