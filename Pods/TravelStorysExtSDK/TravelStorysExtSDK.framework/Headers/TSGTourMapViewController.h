//
//  TSGTourMapViewController.h
//  TravelStorysExtSDK
//
//  Created by David Worth on 4/28/19.
//  Copyright © 2019 TravelStorysGPS, LLC. All rights reserved.
//

#import <CoreLocation/CoreLocation.h>
#import <MapKit/MapKit.h>
#import <UIKit/UIKit.h>

#import "ITSGViewController.h"

#import "TSGAudioManager.h"

#import "TSGBoilerViewSupport.h"
#import "TSGCircleActivityView.h"
#import "TSGDatabase.h"
#import "TSGGeotag.h"
#import "TSGNowPlayingView.h"
#import "TSGRoute.h"
#import "TSGShorthand.h"
#import "TSGTourMapperView.h"
#import "TSGTrack.h"
#import "TSGTriggerLogic.h"
#import "TSGUIColor+Hex.h"

#import "TSGAnalyticsManager.h"


NS_ASSUME_NONNULL_BEGIN



@interface TSGTourMapViewController : UIViewController <CLLocationManagerDelegate, TSGTourMapperDelegate, ITSGViewController, TSGAudioManagerSubscriber, NowPlayingViewDelegate, TSGDebugTextDelegateListener>
{
    IBOutlet UIView* loadingView;
    IBOutlet UIImageView* loadingBackgroundView;
    IBOutlet UILabel* loadingTitleLabel;
    IBOutlet TSGCircleActivityView* loadingActivityView;
    
    IBOutlet UIView* titleBar;
    IBOutlet UILabel* titleLabel;
    IBOutlet UIButton* titleBackButton;
    IBOutlet UIButton* titleSatelliteButton;
    IBOutlet UIButton* titleConnectButton;
    IBOutlet UIView* titleConnectBackground;
    IBOutlet UIButton* titleMoreButton;
    
    IBOutlet UIButton* connectBarButton;
    IBOutlet UIButton* hideConnectBarButton;
    IBOutlet UIButton* funFactsButton;
    
    IBOutlet UILabel* connectBarLabel;
    
    IBOutlet UIButton* aboutTagButton;
    
    IBOutlet TSGTourMapperView* mapView;
    
    IBOutlet TSGNowPlayingView* nowPlayingView;
    
    IBOutlet NSLayoutConstraint* adHeightConstraint;
    
    CLLocationManager* locationManager;
    
    IBOutlet UILabel* debugZoomLabel;
    IBOutlet UITextView* logTextView;
}

- (void)prepRoute:(TSGRoute*)route withTrackId:(int)trackId;

- (void)showAbout;
- (void)close;
- (void)connect;

@property (nonatomic, readonly) TSGRoute* route;
@property (nonatomic, readonly) TSGTrack* track;
@property (nonatomic, readonly) int trackId;

@property (nonatomic) id<ITSGViewControllerDelegate> delegate;

@end

NS_ASSUME_NONNULL_END
