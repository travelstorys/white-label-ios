//
//  TSGFlatTextField.h
//  TravelStorysExtSDK
//
//  Created by David Worth on 7/12/19.
//  Copyright © 2019 TravelStorysGPS, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface TSGFlatTextField : UITextField

@end

NS_ASSUME_NONNULL_END
