//
//  TSGNSDictionary+JSON.h
//  TravelStorysExtSDK
//
//  Created by David Worth on 3/27/19.
//  Copyright © 2019 TravelStorysGPS, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NSDictionary (JSON)

- (NSData*)toJSON;
- (NSString*)toJSONString;

@end

NS_ASSUME_NONNULL_END
