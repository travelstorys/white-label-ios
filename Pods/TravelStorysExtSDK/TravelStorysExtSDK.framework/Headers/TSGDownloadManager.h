//
//  TSGDownloadManager.h
//  TravelStorysExtSDK
//
//  Created by David Worth on 4/3/19.
//  Copyright © 2019 TravelStorysGPS, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@class TSGDownloadTask, TSGDownloadTaskGroup;

@protocol DownloadProgressDelegate <NSObject>

- (void)progressUpdated:(NSObject*)task withProgress:(float)progress;
- (void)dataFinishedDownloading:(TSGDownloadTask*)task withData:(NSData*)data;
- (void)dataFailedDownloading:(TSGDownloadTask*)task withError:(NSError*)error;
- (void)removeActiveDownload:(TSGDownloadTask*)task;

@optional
- (void)taskComplete:(NSObject*)task;

@end

@interface TSGDownloadManager : NSObject
{
    NSMutableDictionary* registry;
    NSMutableArray* activeDownloads;
}

+ (TSGDownloadManager*)shared;

- (TSGDownloadTaskGroup*)getGroupWithId:(NSString*)identifier;
- (void)registerTask:(TSGDownloadTask*)task withIdentifier:(NSString*)identifier;
- (void)registerTaskGroup:(TSGDownloadTaskGroup*)group withIdentifier:(NSString*)identifier;
- (void)updateTask:(TSGDownloadTask*)task;
- (void)updateGroup:(TSGDownloadTaskGroup*)group;
- (void)clearTask:(TSGDownloadTask*)task;
- (void)clearGroup:(TSGDownloadTaskGroup*)group;

@end

@interface TSGDownloadTask : NSObject <NSURLSessionDataDelegate, NSURLSessionDelegate, NSURLSessionTaskDelegate>
{
    NSMutableData* downloadData;
    float downloadSize;
    float progress;
    int length;
    
    NSMutableArray* listeners;
}

@property (nonatomic) id<DownloadProgressDelegate> delegate;
@property (nonatomic) NSString* url;
@property (nonatomic) NSString* taskID;

@property (readonly) int retryCount;

+ (TSGDownloadTask*)downloadDataFromURL:(NSString*)url withDelegate:(id<DownloadProgressDelegate>)delegate;
- (int)getLength;
- (float)getProgress;
- (float)getDownloadSize;

@end

@interface TSGDownloadTaskGroup : NSObject <DownloadProgressDelegate>

@property (nonatomic) int complete;
@property (nonatomic) int failed;
@property (nonatomic) NSMutableArray<TSGDownloadTask*>* tasks;
@property (nonatomic) int taskCount;
@property (nonatomic) id<DownloadProgressDelegate> delegate;
@property (nonatomic) NSString* groupID;

@property (nonatomic) float totalSize;
@property (nonatomic) float downloadedSize;
@property (nonatomic) BOOL totalSizeCalculated;

+ (TSGDownloadTaskGroup*)downloadDataFromURLs:(NSArray<NSString*>*)urls withDelegate:(id<DownloadProgressDelegate>)delegate;
- (float)progress;

@end

NS_ASSUME_NONNULL_END
