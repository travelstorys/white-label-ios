//
//  TSGGeotagAnnotation.h
//  TravelStorysExtSDK
//
//  Created by David Worth on 4/29/19.
//  Copyright © 2019 TravelStorysGPS, LLC. All rights reserved.
//

#import <MapKit/MapKit.h>

#import "TSGConstants.h"
#import "TSGERO.h"
#import "TSGGeotag.h"
#import "TSGRoute.h"

NS_ASSUME_NONNULL_BEGIN

@interface TSGGeotagAnnotation : MKPointAnnotation

@property BOOL touchable;
@property TSGGeotagPin pinImage;
@property TSGGeotag* geotag;

@end

@interface TSGRouteAnnotation : MKPointAnnotation

@property int routeId;
@property TSGERO* route;

@end

@interface GeotagButton : UIButton

@property TSGGeotagAnnotation* annotation;

@end

@interface RouteButton : UIButton

@property TSGRouteAnnotation* annotation;

@end

NS_ASSUME_NONNULL_END
