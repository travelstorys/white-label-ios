//
//  TSGPermissionSlide.h
//  TravelStorysExtSDK
//
//  Created by David Worth on 4/10/19.
//  Copyright © 2019 TravelStorysGPS, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface TSGPermissionSlide : UIView

@property IBOutlet UILabel* titleLabel;
@property IBOutlet UIImageView* iconView;
@property IBOutlet UILabel* bodyLabel;
@property IBOutlet UIButton* firstButton;
@property IBOutlet UIButton* secondButton;

@end

NS_ASSUME_NONNULL_END
