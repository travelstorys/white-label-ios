//
//  TSGUIColor+Hex.h
//  TravelStorys
//
//  Created by David Worth on 7/4/17.
//  Copyright © 2017 TravelStorysGPS. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (Hex)

/**
 * @brief creates a UIColor object from a
 * hex string. Can be 3, 4, 6, 7, 8, or 9 characters.
 *
 */
+ (UIColor*)colorWithHex:(NSString*)hex;

@end
