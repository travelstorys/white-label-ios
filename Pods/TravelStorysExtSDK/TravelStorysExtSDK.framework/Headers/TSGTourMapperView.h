//
//  TSGTourMapperView.h
//  TravelStorysExtSDK
//
//  Created by David Worth on 4/28/19.
//  Copyright © 2019 TravelStorysGPS, LLC. All rights reserved.
//

#import <MapKit/MapKit.h>

#import "TSGDatabase.h"
#import "TSGDataManager.h"
#import "TSGDownloadManager.h"
#import "TSGGeotag.h"
#import "TSGGeotagAnnotation.h"
#import "TSGRoute.h"
#import "TSGTrack.h"

NS_ASSUME_NONNULL_BEGIN

@protocol TSGTourMapperDelegate <NSObject>

- (void)annotationClicked:(TSGGeotagAnnotation*)annotation;
- (void)zoomChanged:(float)zoomScale;

@end

@interface TSGTourMapperView : MKMapView <MKMapViewDelegate>

- (void)loadRoute:(TSGRoute*)route withTrack:(TSGTrack*)track;
- (void)refreshTriggerOverlays;

@property (nonatomic) id<TSGTourMapperDelegate> mapperDelegate;

@property (nonatomic, readonly) TSGRoute* route;
@property (nonatomic, readonly) TSGTrack* track;

@property (nonatomic) BOOL hideButtons;

@end

NS_ASSUME_NONNULL_END
