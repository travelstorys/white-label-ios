//
//  TSGBundleSelectView.h
//  TravelStorysExtSDK
//
//  Created by David Worth on 8/6/19.
//  Copyright © 2019 TravelStorysGPS, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "TSGShorthand.h"
#import "TSGUIColor+Hex.h"
#import "TSGPremiumUtils.h"

NS_ASSUME_NONNULL_BEGIN

@protocol TSGBundleSelectDelegate <NSObject>

- (void)bundleSelected:(BOOL)status;
- (void)bundleDisabled;

@end

@interface TSGBundleSelectView : UIView
{
    IBOutlet UIImageView* checkmarkImage;
    IBOutlet UILabel* bundleAdLabel;
    IBOutlet UIButton* bundleSelectButton;
    
    IBOutlet UIScrollView* listingView;
    
    NSMutableArray* bundleViews;
}

@property (nonatomic) id<TSGBundleSelectDelegate> delegate;
@property (nonatomic) NSString* productIDs;

- (void)hide;
- (void)setBundleSelected:(BOOL)bundleSelected;
- (void)loadWithProduct:(NSDictionary*)product withBundle:(NSDictionary*)bundle withBundleProducts:(NSArray<NSDictionary*>*)bundleProducts;

@end

NS_ASSUME_NONNULL_END
