//
//  TSGManageListItem.h
//  TravelStorysExtSDK
//
//  Created by David Worth on 6/26/19.
//  Copyright © 2019 TravelStorysGPS, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@class TSGManageListItem;
@protocol TSGManageListItemDelegate <NSObject>

- (void)itemDeleted:(TSGManageListItem*)sender;

@end

@interface TSGManageListItem : UIView
{
    IBOutlet UIButton* deleteButton;
    IBOutlet UILabel* titleLabel;
    IBOutlet UILabel* sizeLabel;
    
    IBOutlet UIImageView* imageView;
    
    IBOutlet UIView* topView;
}

- (IBAction)swipeLeft:(id)sender;
- (IBAction)swipeRight:(id)sender;

- (void)loadWithTitle:(NSString*)title withSize:(NSString*)size withImage:(UIImage*)image withKey:(NSString*)key;

@property (nonatomic) id<TSGManageListItemDelegate> delegate;
@property (nonatomic) NSString* key;

@end

NS_ASSUME_NONNULL_END
