//
//  TSGModalWebViewController.h
//  TravelStorysExtSDK
//
//  Created by David Worth on 7/22/19.
//  Copyright © 2019 TravelStorysGPS, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "TSGConstants.h"
#import "TSGDeepLinkHandler.h"
#import "ITSGViewController.h"
#import "TSGShorthand.h"

#import "TSGAnalyticsManager.h"

NS_ASSUME_NONNULL_BEGIN

@interface TSGModalWebViewController : UIViewController <UIWebViewDelegate, TSGDeepLinkResponder, ITSGViewController>

- (void)setup;

- (void)loadURL:(NSURL*)url;

@property (assign) IBOutlet UIButton* dismissButton;
@property (assign) IBOutlet UITextField* urlField;

@property (assign) IBOutlet UIWebView* uwebView;

@property (nonatomic) id localParent;

@end

NS_ASSUME_NONNULL_END
