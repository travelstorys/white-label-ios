//
//  TSGMoreOptionsViewController.h
//  TravelStorysExtSDK
//
//  Created by David Worth on 5/15/19.
//  Copyright © 2019 TravelStorysGPS, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "TSGBoilerViewSupport.h"
#import "TSGDatabase.h"
#import "TSGDataManager.h"
#import "ITSGViewController.h"
#import "TSGLoginViewController.h"
#import "TSGSyncViewController.h"
#import "TSGManageViewController.h"
#import "TSGPasscodeViewController.h"
#import "TSGPreviewViewController.h"

#import "TSGIconListItem.h"
#import "TSGIconListView.h"

#import "TSGShorthand.h"

#import "TSGAnalyticsManager.h"

NS_ASSUME_NONNULL_BEGIN

@interface TSGMoreOptionsViewController : UIViewController <ITSGViewController, TSGIconListViewDelegate, TSGPasscodeDelegate, TSGLoginDelegate, TSGSyncDelegate>
{
    IBOutlet UIButton* titleBackButton;
    IBOutlet UIButton* titleShareButton;
    
    IBOutlet TSGIconListView* listView;
    
    IBOutlet UIImageView* backgroundImageView;
    
    TSGIconListItem* loginItem;
    TSGIconListItem* syncItem;
}

@property id<ITSGViewControllerDelegate> delegate;

- (void)close;
- (void)getPreviewControllerForRoute:(TSGERO*)route;

@end

NS_ASSUME_NONNULL_END
