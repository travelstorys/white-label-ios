//
//  TSGIconListItem.h
//  TravelStorysExtSDK
//
//  Created by David Worth on 5/15/19.
//  Copyright © 2019 TravelStorysGPS, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "TSGShorthand.h"

NS_ASSUME_NONNULL_BEGIN

typedef enum TSGIconListItemTarget : UInt8 {
    IconTargetManageDownloads,
    IconTargetContactUs,
    
    IconTargetTermsOfService,
    IconTargetPrivacyPolicy,
    
    IconTargetLogIn,
    IconTargetSync,
    IconTargetResetTours,
    
    IconTargetDeveloperMenu,
    IconTargetPasscode,
    
    IconTargetSponsors,
    IconTargetPartners,
    IconTargetArtists,
    IconTargetSupport,
    
    IconTargetCustom
} TSGIconListItemTarget;

IB_DESIGNABLE
@interface TSGIconListItem : UIButton

@property (nonatomic) IBInspectable UIImage* iconImage;
@property (nonatomic) IBInspectable NSString* title;
@property (nonatomic) IBInspectable BOOL gapAbove;

- (id)initWithImage:(UIImage*)iconImage withTitle:(NSString*)title withGap:(BOOL)gapAbove;

@end

NS_ASSUME_NONNULL_END
