//
//  SSScoringIndex.h
//  ScoreSearch
//
//  Created by David Worth on 9/11/17.
//  Copyright © 2017 TravelStorysGPS. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "SSConstants.h"

@interface SSScoringIndex : NSObject

@property (nonatomic) SSIndex index;
@property (nonatomic) SSMode mode;
@property (nonatomic) id match;
@property (nonatomic) NSDictionary* options;

- (float)evaluateInput:(id)input;

@end
