//
//  TSGShorthand.h
//  TravelStorysExtSDK
//
//  Created by David Worth on 3/20/19.
//  Copyright © 2019 TravelStorysGPS, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

#define NSLog(FORMAT, ...) sendToFile([NSString stringWithFormat:FORMAT, ##__VA_ARGS__]);

@protocol TSGDebugTextDelegateListener <NSObject>

- (void)debugLog:(NSString*)logText;

@end

typedef enum TSGVersionComparison : UInt8{
    
    ALessThanB,
    Equal,
    BLessThanA
    
} TSGVersionComparison;

@interface TSGShorthand : NSObject



void d_main(dispatch_block_t block);
void d_default(dispatch_block_t block);
void d_low(dispatch_block_t block);
void d_high(dispatch_block_t block);
void d_after(dispatch_block_t block, NSTimeInterval seconds);
void d_after_default(dispatch_block_t block, NSTimeInterval seconds);

+(void)setDelegate:(id<TSGDebugTextDelegateListener>)delegate;

void sendToFile(NSString* log);

BOOL hasNotch(void);

TSGVersionComparison compareVersionAToB(NSString* A, NSString* B);

int randomIntInRange(int lowerBound, int upperBound);

NSString* prettyPrintDuration(int length);
NSString* prettifyTime(int time);

UIImage* imageWithView(UIView* view);
UIImage* grayscaleImage(UIImage* image);

NSLayoutConstraint* leadingConstraint(UIView* v, UIView* w, float c);
NSLayoutConstraint* trailingConstraint(UIView* v, UIView* w, float c);
NSLayoutConstraint* topConstraint(UIView* v, UIView* w, float c);
NSLayoutConstraint* bottomConstraint(UIView* v, UIView* w, float c);
NSLayoutConstraint* heightConstraint(UIView* v, float c);
NSLayoutConstraint* widthConstraint(UIView* v, float c);




@end

NS_ASSUME_NONNULL_END
