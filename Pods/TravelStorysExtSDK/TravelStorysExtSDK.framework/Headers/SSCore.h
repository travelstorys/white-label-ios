//
//  SSCore.h
//  ScoreSearch
//
//  Created by David Worth on 9/11/17.
//  Copyright © 2017 TravelStorysGPS. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "SSConstants.h"
#import "SSScoringIndex.h"

@interface SSCore : NSObject
{
    NSMutableArray* indices;
}

@property (nonatomic) SSMethod scoringMethod;

- (void)addScoringIndex:(SSIndex)index withMode:(SSMode)mode withMatch:(id)match withOptions:(NSDictionary*)options withTarget:(NSString*)target withValue:(int)value;

- (void)removeAllIndexes;

- (NSArray*)evaluateSearch:(NSArray*)rows withThreshold:(float)matchPercent sortByScore:(BOOL)sort;

@end
