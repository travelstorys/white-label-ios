//
//  TSGManageViewController.h
//  TravelStorysExtSDK
//
//  Created by David Worth on 6/26/19.
//  Copyright © 2019 TravelStorysGPS, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "TSGBoilerViewSupport.h"
#import "TSGDatabase.h"
#import "TSGDataManager.h"
#import "ITSGViewController.h"
#import "TSGManageListItem.h"

#import "TSGAnalyticsManager.h"

NS_ASSUME_NONNULL_BEGIN

@interface TSGManageViewController : UIViewController<ITSGViewController, TSGManageListItemDelegate>
{
    IBOutlet UIButton* titleBackButton;
    
    IBOutlet UIImageView* backgroundImageView;
    
    IBOutlet UIScrollView* scrollview;
}

- (void)close;

@end

NS_ASSUME_NONNULL_END
