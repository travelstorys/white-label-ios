//
//  TSGNSString+Hash.h
//  TravelStorysExtSDK
//
//  Created by David Worth on 3/27/19.
//  Copyright © 2019 TravelStorysGPS, LLC. All rights reserved.
//

#import <CommonCrypto/CommonHMAC.h>
#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NSString (Hash)

- (NSString*)MD5String;
- (NSString*)removeString:(NSString *)remove;
- (NSString*)removeFrontWhitespace;

@end

NS_ASSUME_NONNULL_END
