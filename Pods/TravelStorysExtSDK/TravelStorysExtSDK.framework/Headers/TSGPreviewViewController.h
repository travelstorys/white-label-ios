//
//  TSGPreviewViewController.h
//  TravelStorysExtSDK
//
//  Created by David Worth on 5/13/19.
//  Copyright © 2019 TravelStorysGPS, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "ITSGViewController.h"

#import "TSGBoilerViewSupport.h"
#import "TSGCircleProgressView.h"
#import "TSGCRO.h"
#import "TSGDatabase.h"
#import "TSGDownloadManager.h"
#import "TSGERO.h"
#import "TSGGeotag.h"
#import "TSGPriceLabel.h"
#import "TSGRoute.h"
#import "TSGSocialManager.h"
#import "TSGSplitTabView.h"
#import "TSGTourMapperView.h"

#import "TSGPurchaseViewController.h"

#import "TSGAnalyticsManager.h"

NS_ASSUME_NONNULL_BEGIN

@interface TSGPreviewViewController : UIViewController <TSGSplitTabViewDelegate, ITSGViewController, DownloadProgressDelegate, TSGPurchaseDelegate>
{
    IBOutlet UIButton* titleBackButton;
    IBOutlet UIButton* titleShareButton;
    
    IBOutlet TSGSplitTabView* tabView;
    
    IBOutlet UIImageView* firstImageView;
    IBOutlet UIImageView* secondImageView;
    
    IBOutlet UIImageView* orgImageView;
    IBOutlet UILabel* routeTitleLabel;
    IBOutlet UILabel* routePresentedLabel;
    
    IBOutlet UIImageView* locationImage;
    IBOutlet UIImageView* timeImage;
    
    IBOutlet UILabel* locationLabel;
    IBOutlet UILabel* timeLabel;
    
    IBOutlet TSGPriceLabel* routePriceLabel;
    
    IBOutlet UILabel* summaryLabel;
    IBOutlet UIButton* actionButton;
    
    //TSGCircleProgressView* progressView;
    
    IBOutlet TSGTourMapperView* mapView;
    
    IBOutlet UIImageView* premiumBackground;
    IBOutlet UILabel* premiumLabel;
    
    IBOutlet UIVisualEffectView* downloadingView;
    IBOutlet UIProgressView* progressView;
    IBOutlet UILabel* downloadingTitleLabel;
    IBOutlet UILabel* downloadingSizeLabel;
}

- (void)loadRoute:(TSGERO*)route;
- (void)playAction;
- (void)close;

@property (nonatomic, readonly) TSGERO* route;

@property id<ITSGViewControllerDelegate> delegate;

@end

NS_ASSUME_NONNULL_END
