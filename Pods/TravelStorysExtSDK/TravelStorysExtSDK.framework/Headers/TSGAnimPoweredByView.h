//
//  TSGAnimPoweredByView.h
//  TravelStorysExtSDK
//
//  Created by David Worth on 3/19/19.
//  Copyright © 2019 TravelStorysGPS, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface TSGAnimPoweredByView : UIView

- (void)startAnimation;

@end

NS_ASSUME_NONNULL_END
