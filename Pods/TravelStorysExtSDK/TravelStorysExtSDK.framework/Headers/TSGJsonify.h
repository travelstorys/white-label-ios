//
//  TSGJsonify.h
//  TravelStorysExtSDK
//
//  Created by David Worth on 3/27/19.
//  Copyright © 2019 TravelStorysGPS, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@protocol TSGJsonable <NSObject>

- (NSDictionary*)jsonify;
- (id)initWithJsonify:(NSDictionary*)jsonify;

@end

@interface TSGJsonify : NSObject

+ (NSDictionary*)dictionaryWithDictionary:(NSDictionary*)dictionary forClass:(Class)senderClass;
+ (NSArray*)jsonifyArray:(NSArray*)array;
+ (NSDictionary*)jsonifyDictionary:(NSDictionary*)dictionary;

+ (NSArray*)dejsonifyArray:(NSArray*)array;
+ (NSDictionary*)dejsonifyDictionary:(NSDictionary*)dictionary;

@end

NS_ASSUME_NONNULL_END
