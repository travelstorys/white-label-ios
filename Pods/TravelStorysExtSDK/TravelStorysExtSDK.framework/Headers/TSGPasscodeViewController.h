//
//  TSGPasscodeViewController.h
//  TravelStorysExtSDK
//
//  Created by David Worth on 5/16/19.
//  Copyright © 2019 TravelStorysGPS, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "TSGBoilerViewSupport.h"
#import "ITSGViewController.h"
#import "TSGPassDotsView.h"
#import "TSGShorthand.h"

#import "TSGAnalyticsManager.h"

NS_ASSUME_NONNULL_BEGIN

@class TSGPasscodeViewController;

@protocol TSGPasscodeDelegate <NSObject>

- (void)passcodeEntered:(NSString*)passcode withSender:(TSGPasscodeViewController*)sender;

@end

typedef enum TSGPasscodeMode : UInt8
{
    PasscodeDefault,
    PasscodeDeveloper
} TSGPasscodeMode;

@interface TSGPasscodeViewController : UIViewController <ITSGViewController>
{
    IBOutlet UIImageView* backgroundImageView;
    
    IBOutlet UIButton* titleBackButton;
    IBOutlet UILabel* titleLabel;
    
    IBOutlet UILabel* passcodeLabel;
    IBOutlet UILabel* passcodeDetailLabel;
    
    IBOutlet TSGPassDotsView* dots;
    IBOutlet UITextField* inputField;
    
    IBOutlet UIView* developerView;
    IBOutlet UIView* passcodeView;
    
    IBOutlet UISwitch* developerDebugMenuSlider;
    IBOutlet UISwitch* developerDebugDBSlider;
}

- (void)resetAsWrong;
- (void)resetAsRight;

- (void)showDeveloperMenu;

@property (nonatomic) id<TSGPasscodeDelegate> pDelegate;
@property (nonatomic) TSGPasscodeMode mode;

@end

NS_ASSUME_NONNULL_END
