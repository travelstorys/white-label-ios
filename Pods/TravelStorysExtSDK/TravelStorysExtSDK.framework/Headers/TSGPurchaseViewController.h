//
//  TSGPurchaseViewController.h
//  TravelStorysExtSDK
//
//  Created by David Worth on 7/23/19.
//  Copyright © 2019 TravelStorysGPS, LLC. All rights reserved.
//

#import <CoreLocation/CoreLocation.h>
#import <MapKit/MapKit.h>
#import <UIKit/UIKit.h>

#import "TSGBoilerViewSupport.h"
#import "TSGConstants.h"
#import "TSGDeepLinkHandler.h"
#import "ITSGViewController.h"
#import "KMLParser.h"
#import "Reachability.h"
#import "TSGShorthand.h"
#import "TSGMKMapView+Zoom.h"

#import "TSGBundleSelectView.h"
#import "TSGCircleActivityView.h"
#import "TSGPriceLabel.h"
#import "TSGRoute.h"

#import "TSGDatabase.h"
#import "TSGDataManager.h"
#import "TSGIAPManager.h"

#import "TSGLoginViewController.h"

#import "TSGPremiumUtils.h"
#import "TravelStorys.h"

#import "TSGAnalyticsManager.h"

NS_ASSUME_NONNULL_BEGIN

@protocol TSGPurchaseDelegate <NSObject>

- (void)purchaseComplete:(NSDictionary*)ownership;

@end

@interface TSGPurchaseViewController : UIViewController <ITSGViewController, TSGLoginDelegate, MKMapViewDelegate, TSGIAPManagerDelegate, TSGBundleSelectDelegate, UIAlertViewDelegate>
{
    IBOutlet UIButton* closeButton;
    
    IBOutlet TSGPriceLabel* priceLabel;
    
    IBOutlet UIView* checkingView;
    IBOutlet UILabel* checkingLabel;
    IBOutlet TSGCircleActivityView* checkingActivityView;
    
    IBOutlet UIView* presentedView;
    IBOutlet TSGBundleSelectView* bundleView;
    IBOutlet MKMapView* mapView;
    IBOutlet UITextView* summaryView;
    IBOutlet UIView* purchaseBackground;
    IBOutlet UILabel* purchaseFinalLabel;
    IBOutlet UIButton* purchaseButton;
    
    IBOutlet UIView* thanksView;
    IBOutlet UIButton* thanksCloseButton;
    
    IBOutlet NSLayoutConstraint* bundleHeightConstraint;
    
    NSMutableArray<KMLParser*>* kmlParsers;
    
    NSMutableArray* bundleProducts;
    
    int basePrice;
    int bundlePrice;
    BOOL buyingBundle;
    
    int lifetime;
    
    int purchasePrice;
    NSString* purchaseIDs;
    int productID;
    
    NSDictionary* savedProduct;
    NSDictionary* savedBundle;
    
    NSString* initialSummaryValue;
    
    NSDictionary* activePromoCode;
}

@property (nonatomic) TSGRoute* route;
@property (nonatomic) id<TSGPurchaseDelegate> delegate;

- (void)loadProductInfo:(TSGRoute*)route;

@end

NS_ASSUME_NONNULL_END
