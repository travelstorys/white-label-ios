//
//  TSGPreloadViewController.h
//  TravelStorysExtSDK
//
//  Created by David Worth on 3/19/19.
//  Copyright © 2019 TravelStorysGPS, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "TravelStorys.h"
#import "TSGSessionManager.h"

#import "ITSGViewController.h"
#import "TSGPermissionsViewController.h"

#import "TSGLoadingAnimationView.h"
#import "TSGFlatProgressView.h"

#import "TSGShorthand.h"
#import "Reachability.h"

#import "TSGAnalyticsManager.h"

NS_ASSUME_NONNULL_BEGIN

@interface TSGPreloadViewController : UIViewController<ITSGViewController, TSGSessionManagerDelegate, UIAlertViewDelegate>

@property (nonatomic, assign) IBOutlet TSGLoadingAnimationView* animation;
@property (nonatomic, assign) IBOutlet TSGFlatProgressView* progress;
@property (nonatomic, assign) IBOutlet UIImageView* logoView;
@property (nonatomic, assign) IBOutlet UILabel* mainLabel;
@property (nonatomic, assign) IBOutlet UILabel* subLabel;

@property (nonatomic) id<ITSGViewControllerDelegate> delegate;

- (void)startPreload;
- (void)completeView;

@end

NS_ASSUME_NONNULL_END
