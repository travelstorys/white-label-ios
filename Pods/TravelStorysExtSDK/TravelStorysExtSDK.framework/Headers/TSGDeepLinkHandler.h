//
//  TSGDeepLinkHandler.h
//  TravelStorysExtSDK
//
//  Created by David Worth on 7/22/19.
//  Copyright © 2019 TravelStorysGPS, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

#import "TSGConstants.h"
#import "TSGERO.h"

NS_ASSUME_NONNULL_BEGIN

/**
 * Protocol TSGDeepLinkResponder
 * Provides an interface for handling
 * specific deep link events
 *
 */
@protocol TSGDeepLinkResponder <NSObject>

@optional

- (void)routeRequested:(TSGERO*)route;

/**
 * void redeemCodeSuccess
 * @brief called when a redeem-code-success deep link
 *  is called.
 *
 * @param package
 *  NSString*
 *  Contains the package IDs for the redeemed code.
 *
 */
- (void)redeemCodeSuccess:(NSString*)package;

/**
 * void returnCode
 * @brief called when a return-code deep link
 *  is called.
 *
 */
- (void)returnCode;

@end

@interface TSGDeepLinkHandler : NSObject

+ (void)setDeepLinkScheme:(NSString*)scheme;
+ (NSString*)getDeepLinkScheme;

+ (void)setAppLoaded:(BOOL)loaded;
+ (BOOL)getAppLoaded;

+ (BOOL)application:(UIApplication *)app openURL:(NSURL *)url;

@end

NS_ASSUME_NONNULL_END
