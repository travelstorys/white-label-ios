//
//  TSGBoilerViewSupport.h
//  TravelStorysExtSDK
//
//  Created by David Worth on 4/28/19.
//  Copyright © 2019 TravelStorysGPS, LLC. All rights reserved.
//

#ifndef BoilerViewSupport_h
#define BoilerViewSupport_h

typedef enum TSGBoilerplateValue : UInt8 {
    
    BoilerLogo,
    BoilerLogoTrans,
    
    BoilerDiscoveryBackground,
    
    BoilerLaunchLogo,
    BoilerLaunchTitle,
    BoilerLaunchSubtitle,
    BoilerLaunchDarkMode,
    
    BoilerPermissions,
    BoilerPermissionsWelcome,
    
    BoilerColorPrimary,
    BoilerColorSecondary,
    BoilerColorTertiary,
    BoilerColorNeutral,
    BoilerColorNeutral2,
    
    BoilerOptionsBackground,
    BoilerOptionsContactURL,
    BoilerOptionsPrivacyURL,
    BoilerOptionsTermsURL,
    
    BoilerAssetDownloadSmall,
    BoilerAssetDownloadMedium,
    BoilerAssetPlaySmall,
    BoilerAssetPlayMedium,
    
    BoilerTSGDeepLinkScheme
    
} TSGBoilerplateValue;

@protocol TSGBoilerViewSupport

- (id)getObjectFromBoiler:(TSGBoilerplateValue)value;

@end

#endif /* BoilerViewSupport_h */
