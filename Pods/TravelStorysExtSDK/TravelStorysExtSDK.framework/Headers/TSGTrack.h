//
//  TSGTrack.h
//  TravelStorysExtSDK
//
//  Created by David Worth on 3/26/19.
//  Copyright © 2019 TravelStorysGPS, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "TSGJsonify.h"

NS_ASSUME_NONNULL_BEGIN

/**
 *@brief Container for TSG TSGTrack objects.
 *
 */
@interface TSGTrack : NSObject <TSGJsonable>

/**
 *@brief Unique TSG TSGRoute ID this track belongs to.
 *
 */
@property (nonatomic) int routeID;

/**
 *@brief Unique TSG TSGTrack ID for this track.
 *
 */
@property (nonatomic) int trackID;

/**
 *@brief True if this track is active.
 *
 */
@property (nonatomic) BOOL live;

/**
 *@brief Title of the track.
 *
 */
@property (nonatomic) NSString* title;

/**
 *@brief Summary of the track.
 *
 */
@property (nonatomic) NSString* descriptionText;

/**
 *@brief Name of the custom icon for the track.
 *
 */
@property (nonatomic) NSString* icon;

/**
 *@brief Legend contents of the KML file for this track.
 *
 */
@property (nonatomic) NSString* kmlBanner;

/**
 *@brief Asset ID of a custom intro for this track.
 *
 */
@property (nonatomic) int introID;

@end

NS_ASSUME_NONNULL_END
