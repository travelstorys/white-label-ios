//
//  TSGGeotag.h
//  TravelStorysExtSDK
//
//  Created by David Worth on 3/25/19.
//  Copyright © 2019 TravelStorysGPS, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

#import "TSGConstants.h"
#import "TSGGeotagImage.h"

#import "TSGJsonify.h"

NS_ASSUME_NONNULL_BEGIN

@interface TSGGeotag : NSObject <TSGJsonable>

#pragma mark -
#pragma mark Basic TSGGeotag Info

/**
 *@brief Internal TSG ID number of the TSGGeotag.
 *
 */
@property (nonatomic) int geotagId;

/**
 *@brief Unique alphanumeric key of the TSGGeotag.
 *
 */
@property (nonatomic) NSString* geotagKey;

/**
 *@brief Title of the TSGGeotag.
 *
 */
@property (nonatomic) NSString* title;

/**
 @brief Subtitle of the TSGGeotag.
 *
 */
@property (nonatomic) NSString* subtitle;

/**
 *@brief True if the tag should be used.
 *
 */
@property (nonatomic) BOOL live;

/**
 *@brief ID of the pin image for the TSGGeotag.
 *
 */
@property (nonatomic) TSGGeotagPin pin;

/**
 *@brief Used if the TSGGeotag is using a custom pin image.
 * Values should be greater than 50.
 *
 */
@property (nonatomic) int customPin;

/**
 *@brief True if the TSGGeotag should show a callout when tapped.
 *
 */
@property (nonatomic) BOOL canTouch;

/**
 *@brief The ID number of the track this TSGGeotag belongs to.
 *
 */
@property (nonatomic) int trackID;

/**
 *@brief True if the TSGGeotag should not be rendered on the map.
 *
 */
@property (nonatomic) BOOL hidden;

#pragma mark -
#pragma mark Triggering

/**
 *@brief The latitude of the Geotag's coordinate
 *
 */
@property (nonatomic) float latitude;

/**
 *@brief The longitude of the Geotag's coordinate
 *
 */
@property (nonatomic) float longitude;

/**
 *@brief The Geotag's trigger mode. See TSGConstants.h for values.
 *
 */
@property (nonatomic) TSGGeotagTriggerMode triggerMode;

/**
 *@brief True if the TSGGeotag should only automatically trigger once.
 *
 */
@property (nonatomic) BOOL triggerOnce;

/**
 *@brief True if the TSGGeotag has been triggered.
 *
 */
@property (nonatomic) BOOL hasTriggered;

/**
 *@brief True if the TSGGeotag should ignore all triggering events.
 *
 */
@property (nonatomic) BOOL disabled;

/**
 *@brief True if the TSGGeotag has Prev Set conditions.
 *
 */
@property (nonatomic) BOOL enablePrevSet;

/**
 *@brief Array of Geotags that make up this tag's Prev Set conditions.
 *
 */
@property (nonatomic) NSArray<TSGGeotag*>* prevSet;

/**
 *@brief Array of TSGGeotag IDs that make up this tag's Prev Set conditions.
 *
 */
@property (nonatomic) NSArray<NSString*>* prevSetIDs;

/**
 *@brief True if Tour Memory should be applied for this tag's Prev Set conditions.
 *
 */
@property (nonatomic) BOOL shouldStartPrevSet;

/**
 *@brief True if the TSGGeotag has Next Set conditions.
 *
 */
@property (nonatomic) BOOL enableNextSet;

/**
 *@brief Array of Geotags that make up this tag's Next Set conditions.
 *
 */
@property (nonatomic) NSArray<TSGGeotag*>* nextSet;

/**
 *@brief Array of TSGGeotag IDs that make up this tag's Next Set conditions.
 *
 */
@property (nonatomic) NSArray<NSString*>* nextSetIDs;

#pragma mark Radial Options

/**
 *@brief Radius (in meters) of the tag's radial trigger.
 *
 */
@property (nonatomic) float radius;

/**
 *@brief True if the tag has a directional trigger.
 *
 */
@property (nonatomic) BOOL directional;

#pragma mark Directional Options

/**
 *@brief Center of the tag's directional trigger angle. (DEGREES)
 *@details This uses the standard math coordinate system, where 0 represents +X,
 * and increasing the angle moves counterclockwise. 90 is +Y, 180 is -X, 270 is -Y, etc.
 *
 */
@property (nonatomic) float triggerAngleCenter;

/**
 *@brief Width of the tag's directional trigger angle. (DEGREES)
 *
 */
@property (nonatomic) float triggerAngleWidth;
/**
 *@brief True if the tag should not trigger because of direction.
 *@details The standard triggering system uses this variable to mark a tag
 * as untriggerable when the user enters the tag's radius, but did not enter
 * inside the directional wedge.
 *
 */
@property (nonatomic) BOOL disabledForDirection;

#pragma mark Polygon Options

/**
 *@brief Array of CGPoints marking the vertices of the tag's polygon trigger.
 *
 */
@property (nonatomic) NSArray<NSValue*>* polygon;

#pragma mark Beacon Options

/**
 *@brief UID of the beacon the geotag responds to. Use _radius for the expected distance from the beacon.
 *
 */
@property (nonatomic) NSString* beacon;

#pragma mark -
#pragma mark Files

/**
 *@brief URL of the Geotag's text file.
 *@details In custom implementations, you can use this variable to hold raw text as well.
 *
 */
@property (nonatomic) NSString* text;

/**
 *@brief URL of the Geotag's audio file.
 *
 */
@property (nonatomic) NSString* audioURL;

/**
 *@brief URL of the Geotag's video file.
 *@details This currently only supports YouTube embed links.
 *
 */
@property (nonatomic) NSString* videoURL;

/**
 *@brief Array of the Geotag's images.
 *
 */
@property (nonatomic) NSArray<TSGGeotagImage*>* images;

#pragma mark -
#pragma mark Maps

/**
 *@brief Weak reference to map annotation
 *
 */
@property (nonatomic, weak) id<MKAnnotation> annotation;

/**
 *@brief Reference to map overlay renderer
 *
 */
@property (nonatomic) MKOverlayRenderer* renderer;

/**
 *@brief Returns the name of the pin image used by this TSGGeotag.
 *
 */
- (NSString*)pinImage;

/**
 *@brief Returns the name of the pin image matching the given pin.
 *
 */
+ (NSString*)pinImage:(TSGGeotagPin)pin;

@end

NS_ASSUME_NONNULL_END
