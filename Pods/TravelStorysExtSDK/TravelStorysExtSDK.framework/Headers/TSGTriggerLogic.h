//
//  TSGTriggerLogic.h
//  TravelStorysExtSDK
//
//  Created by David Worth on 4/28/19.
//  Copyright © 2019 TravelStorysGPS, LLC. All rights reserved.
//

#import <CoreLocation/CoreLocation.h>
#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

#import "TSGGeotag.h"
#import "TSGRoute.h"
#import "TSGAudioManager.h"
#import "TSGShorthand.h"


NS_ASSUME_NONNULL_BEGIN

@interface TSGTriggerLogic : NSObject
{
    NSString* nowPlayingTitle;
}

+ (void)checkGeotagTriggers:(NSArray<TSGGeotag*>*)geotags onTrack:(int)trackID atLocation:(CLLocation*)location callback:(void(^)(TSGGeotag* triggered))callback;

+ (TSGGeotag*)lastTriggered;

+ (void)setStatus:(BOOL)status;



@end

NS_ASSUME_NONNULL_END
