//
//  TSGMapOverlay.h
//  TravelStorysExtSDK
//
//  Created by Matt Ellison on 11/14/19.
//  Copyright © 2019 TravelStorysGPS, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "TSGJsonify.h"

#ifndef TSGMapOverlay_h
#define TSGMapOverlay_h

@interface TSGMapOverlay : NSObject <TSGJsonable>

@property int mapID;
@property int routeID;
@property NSString* url;
@property float lat;
@property float lng;
@property int width;
@property int height;
@property float scale;
@property NSDictionary* constraints;
@property NSString* flags;
//@property UIImage* image;

@end
#endif /* TSGMapOverlay_h */
