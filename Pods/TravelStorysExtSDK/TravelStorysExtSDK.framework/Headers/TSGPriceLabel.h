//
//  TSGPriceLabel.h
//  TravelStorysExtSDK
//
//  Created by David Worth on 5/14/19.
//  Copyright © 2019 TravelStorysGPS, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

IB_DESIGNABLE
@interface TSGPriceLabel : UIView

@property (nonatomic) IBInspectable NSString* text;

@end

NS_ASSUME_NONNULL_END
