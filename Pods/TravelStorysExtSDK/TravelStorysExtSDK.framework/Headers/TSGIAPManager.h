//
//  TSGIAPManager.h
//  TravelStorysExtSDK
//
//  Created by David Worth on 8/9/19.
//  Copyright © 2019 TravelStorysGPS, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

#import <StoreKit/StoreKit.h>

NS_ASSUME_NONNULL_BEGIN

@protocol TSGIAPManagerDelegate

- (void)purchaseComplete:(NSString*)iapID;
- (void)purchaseCancelled:(NSString*)iapID;
- (void)purchaseFailed:(NSString*)iapID;

@end

@interface TSGIAPManager : NSObject <SKProductsRequestDelegate, SKRequestDelegate, SKPaymentTransactionObserver>
{
    SKProductsRequest* productRequest;
    SKProductsResponse* productResponse;
    SKProduct* product;
    SKPayment* payment;
    
    NSString* currentProductID;
}

@property (nonatomic) id<TSGIAPManagerDelegate> delegate;

+ (TSGIAPManager*)sharedManager;
- (void)startPurchase:(NSString*)productID;

@end

NS_ASSUME_NONNULL_END
