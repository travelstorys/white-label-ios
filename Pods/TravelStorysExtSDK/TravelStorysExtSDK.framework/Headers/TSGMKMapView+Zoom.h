//
//  MKMapView+Zoom.h
//  TravelStorysExtSDK
//
//  Created by David Worth on 10/16/19.
//  Copyright © 2019 TravelStorysGPS, LLC. All rights reserved.
//

#import <MapKit/MapKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface MKMapView (Zoom)

- (void)zoomToFitOverlays:(NSArray *)someOverlays animated:(BOOL)animated insetProportion:(CGFloat)insetProportion;

@end

NS_ASSUME_NONNULL_END
