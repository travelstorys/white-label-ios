//
//  TSGTrackSelectItem.h
//  TravelStorysExtSDK
//
//  Created by David Worth on 7/24/19.
//  Copyright © 2019 TravelStorysGPS, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "TSGTrack.h"
#import "TSGShorthand.h"

NS_ASSUME_NONNULL_BEGIN

@interface TSGTrackSelectItem : UIButton
{
    IBOutlet UIImageView* iconView;
    IBOutlet UILabel* titleLabel;
    IBOutlet UILabel* descriptionLabel;
}

@property (nonatomic) TSGTrack* track;

+ (TSGTrackSelectItem*)loadFromNib;

@end

NS_ASSUME_NONNULL_END
