//
//  TSGIconListView.h
//  TravelStorysExtSDK
//
//  Created by David Worth on 5/15/19.
//  Copyright © 2019 TravelStorysGPS, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TSGIconListItem.h"

#import "TSGShorthand.h"

NS_ASSUME_NONNULL_BEGIN

@protocol TSGIconListViewDelegate <NSObject>

- (void)iconListItemClicked:(TSGIconListItem*)sender;

@end

IB_DESIGNABLE
@interface TSGIconListView : UIScrollView

@property (nonatomic, readonly) NSMutableArray* items;
@property (nonatomic, assign) IBInspectable id<TSGIconListViewDelegate> ilvDelegate;

- (void)addItem:(TSGIconListItem*)item;
- (void)removeAllItems;

@end

NS_ASSUME_NONNULL_END
